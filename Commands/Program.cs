﻿using L2C.Contracts.Interfaces.Commands;
using L2C.Contracts.Interfaces.Server;
using ServerParams = L2C.Tasks.Server.ServerParams;
using System.ServiceProcess;
using System;
using System.Linq;
using System.ComponentModel;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;
using DLLSharedMethods;
using System.Configuration;
namespace L2C.Tasks
{
    static class Program
    {
        public static ContextMenu menu;
        public static NotifyIcon notificationIcon;
         [STAThread]
        static void Main(string[] args)
        {
            Thread thread = new Thread(Start);
            thread.Start();
            ShowTrayIcon();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run();
        }
        
        private static void Start()
        {
            IServerParams serverParams = new ServerParams();
            serverParams.GetServerParams();
            IServer server = new Server.Server();
            server.SetUpConnection(serverParams);
            while (true)
                server.Listener();
        }

        static void ShowTrayIcon()
        {
            Thread notifyThread = new Thread(
                delegate()
                {
                    menu = new ContextMenu();
                    try
                    {
                        System.Drawing.Icon icon = new System.Drawing.Icon(String.Format("{0}\\icon.ico", ConfigurationManager.AppSettings["currentPath"]));
                        notificationIcon = new NotifyIcon()
                        {
                            Icon = icon,
                            ContextMenu = menu,
                            Text = "L2C - Command Manager"
                        };
                    }
                    catch(Exception e)
                    {
                        LoggerHelper.Log(DateTime.Now + " directory: " + ConfigurationManager.AppSettings["currentPath"] + "error: " + e.StackTrace);
                    }
                    notificationIcon.Visible = true;
                    Application.Run();
                }
            );
            notifyThread.Start();
        }
    }
    //static class Program
    //{
        /*[RunInstaller(true)]
        public partial class ProjectInstaller : System.Configuration.Install.Installer
        {
            ServiceInstaller serviceInstaller;
            protected override void OnBeforeInstall(System.Collections.IDictionary savedState)
            {
                base.OnBeforeInstall(savedState);
                SetServicePropertiesFromCommandLine(serviceInstaller);
            }
            public ProjectInstaller()
            {
                ServiceProcessInstaller serviceProcessInstaller = new ServiceProcessInstaller();
                serviceInstaller = new ServiceInstaller();

                //# Service Account Information
                serviceProcessInstaller.Account = ServiceAccount.LocalSystem;
                serviceProcessInstaller.Username = null;
                serviceProcessInstaller.Password = null;
                serviceInstaller.Description = "Runs in background and processes all the operation coming from L2CPrintingClient";
                serviceInstaller.DisplayName = "L2C - Command Manager";
                serviceInstaller.ServiceName = "L2C - Command Manager";


                //# Service Information
                serviceInstaller.StartType = ServiceStartMode.Automatic;
                this.Installers.Add(serviceProcessInstaller);
                this.Installers.Add(serviceInstaller);
            }

            private void SetServicePropertiesFromCommandLine(ServiceInstaller serviceInstaller)
            {
                string[] commandlineArgs = Environment.GetCommandLineArgs();

                string servicename;
                string servicedisplayname;
                ParseServiceNameSwitches(commandlineArgs, out servicename, out servicedisplayname);

                serviceInstaller.ServiceName = servicename;
                serviceInstaller.DisplayName = servicedisplayname;
            }

            private void ParseServiceNameSwitches(string[] commandlineArgs, out string serviceName, out string serviceDisplayName)
            {
                var servicenameswitch = (from s in commandlineArgs where s.StartsWith("/servicename") select s).FirstOrDefault();
                var servicedisplaynameswitch = (from s in commandlineArgs where s.StartsWith("/servicedisplayname") select s).FirstOrDefault();

                if (servicenameswitch == null)
                    throw new ArgumentException("Argument 'servicename' is missing");
                if (servicedisplaynameswitch == null)
                    throw new ArgumentException("Argument 'servicedisplayname' is missing");
                if (!(servicenameswitch.Contains('=') || servicenameswitch.Split('=').Length < 2))
                    throw new ArgumentNullException("The /servicename switch is malformed");

                if (!(servicedisplaynameswitch.Contains('=') || servicedisplaynameswitch.Split('=').Length < 2))
                    throw new ArgumentNullException("The /servicedisplaynameswitch switch is malformed");

                serviceName = servicenameswitch.Split('=')[1];
                serviceDisplayName = servicedisplaynameswitch.Split('=')[1];

                serviceName = serviceName.Trim('"');
                serviceDisplayName = serviceDisplayName.Trim('"');
            }
        }
        public const string ServiceName = "L2C - Command Manager";
        public class Service : ServiceBase
        {
            public Service()
            {
                ServiceName = ServiceName;
            }
            protected override void OnStart(string[] args)
            {
                Thread thread = new Thread(Start);
                thread.Start();
            }
        }*/
        //static void Main(string[] args)
        //{
            /*if (!Environment.UserInteractive)
                using (var service = new Service())
                    ServiceBase.Run(service);
            else
                Start();*/
            /*IServerParams serverParams = new ServerParams();
            serverParams.GetServerParams();
            IServer server = new Server.Server();
            server.SetUpConnection(serverParams);
            while (true)
                server.Listener();*/
        //}
    
    //}
}
