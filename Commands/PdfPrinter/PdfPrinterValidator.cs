﻿using System.IO;
using L2C.Contracts.Interfaces;

namespace L2C.Tasks.PdfPrinter
{
    class PdfPrinterValidator:IValidator 
    {
        private readonly string[] _tokens;

        public PdfPrinterValidator(string[] tokens)
        {
            _tokens = tokens;
        }
        public bool IsValid()
        {
            return _tokens.Length == 5;
        }
    }
}
