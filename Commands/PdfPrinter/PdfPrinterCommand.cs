﻿using System;
using System.Collections.Generic;
using System.IO;
using Foxit.PDF.Printing;
using L2C.Contracts.Interfaces.Commands;
using MailMerge.Lib;

namespace L2C.Tasks.PdfPrinter
{
    public class PdfPrinterCommand:IPdfPrinterCommand
    {
        private readonly Dictionary<string, string> _cassettoFromTipoCarta;
        public string PrinterName { get; private set; }
        public string FileName { get; private set; }
        public string TraySchemeFile { get; private set; }
        public PdfPrinterCommand(string printerName, string fileName, string traySchemeFile,string cassettoFromTipoCarta)
        {
            _cassettoFromTipoCarta=new Dictionary<string, string>();
            string[] tmp = cassettoFromTipoCarta.Split("/".ToCharArray());
            foreach (string s in tmp)
            {
                string[] t = s.Split(":".ToCharArray());
                if (t.Length == 2 && t[0] != "" && t[1] != "")
                    _cassettoFromTipoCarta[t[0]] = t[1];
            }
            PrinterName = printerName;
            FileName = fileName;
            TraySchemeFile = traySchemeFile;
        }

        public void PrintSimple()
        {
            PrintJob.AddLicense("FPM10NXDAL5KLOw/R/zySdzB2fEl9pS2/aXTs11PEZulHZdRsqWNDBPJIQGw0wszSeNhjCNB4trJjwA17Y3EyImb1UiPQR8N8cAw");
            PrintJob printJob = new PrintJob(PrinterName, FileName);
            printJob.Print();
        }

        static void PrintToDefaultPrinterSimple()
        {
            PrintJob printJob = new PrintJob(Printer.Default, Path.Combine(GetPath(), "LetterPortrait.pdf"));
            printJob.Print();
        }

        static void PrintPageRange()
        {
            PrintJob printJob = new PrintJob(Printer.Default, Path.Combine(GetPath(), "LetterPortrait.pdf"), 1, 1);
            printJob.Print();
        }

        static void PrintFromByteArray()
        {
            byte[] pdfArray = System.IO.File.ReadAllBytes(Path.Combine(GetPath(), "LetterPortrait.pdf"));
            InputPdf inputPdf = new InputPdf(pdfArray);

            PrintJob printJob = new PrintJob(Printer.Default, inputPdf);
            printJob.Print();
        }

        static void PrintWithSettingSimpleOptions()
        {
            PrintJob printJob = new PrintJob(Printer.Default, Path.Combine(GetPath(), "LetterPortrait.pdf"));

            printJob.DocumentName = "Letter Portrait"; // Note: This will default to the Title of the PDF specified in the constructor. If it is blank it will be "Untitled".
            printJob.PrintOptions.SetResolutionByDpi(300); // Note: This will return false if the dpi is not available on the printer and leave the property unchanged.
            printJob.PrintOptions.SetPaperSizeByName("Legal"); // Note: This will return false if the paper is not available on the printer and leave the property unchanged.
            printJob.PrintOptions.SetPaperSourceByName("Tray 3"); // Note: This will return false if the tray is not available on the printer and leave the property unchanged.

            printJob.Print();
        }

        static void PrintSettingAllOptions()
        {
            PrintJob printJob = new PrintJob("PrinterName", Path.Combine(GetPath(), "LetterPortrait.pdf"));

            printJob.DocumentName = "Letter Portrait";
            if (printJob.Printer.Color)
                printJob.PrintOptions.Color = true;

            if (printJob.Printer.Collate)
                printJob.PrintOptions.Collate = true;

            if (printJob.Printer.Duplex)
                printJob.PrintOptions.DuplexMode = DuplexMode.DuplexHorizontal;

            printJob.PrintOptions.Copies = 2;
            printJob.PrintOptions.HorizontalAlign = HorizontalAlign.Left;
            printJob.PrintOptions.Orientation.Type = OrientationType.Portrait;
            printJob.PrintOptions.Scaling = PageScaling.ActualSize; // Can also use AutoPageScaling or PercentagePageScaling objects
            printJob.PrintOptions.PaperSize = printJob.Printer.PaperSizes["Legal"];
            printJob.PrintOptions.PaperSource = printJob.Printer.PaperSources[2];
            printJob.PrintOptions.PrintAnnotations = false;
            printJob.PrintOptions.Resolution = printJob.Printer.Resolutions[0];
            printJob.PrintOptions.VerticalAlign = VerticalAlign.Top;

            printJob.Print();
        }

        static void PrintMultiplePDFs()
        {
            PrintJob printJob = new PrintJob("PrinterName");

            printJob.PrintOptions.Scaling = new AutoPageScaling();

            printJob.Pages.Add(Path.Combine(GetPath(), "LegalPortrait.pdf"));
            printJob.Pages.Add(Path.Combine(GetPath(), "LegalLandscape.pdf"));
            printJob.Pages.Add(Path.Combine(GetPath(), "LetterPortrait.pdf"));
            printJob.Pages.Add(Path.Combine(GetPath(), "LetterLandscape.pdf"));

            printJob.Print();
        }

        public void Execute()
        {
            PrintJob.AddLicense("FPM10NXDAL5KLOw/R/zySdzB2fEl9pS2/aXTs11PEZulHZdRsqWNDBPJIQGw0wszSeNhjCNB4trJjwA17Y3EyImb1UiPQR8N8cAw");
            PrintJob printJob = new PrintJob(PrinterName, FileName);
            printJob.PrintOptions.DuplexMode = DuplexMode.Simplex;
            printJob.PrintOptions.Color = false;
            printJob.PrintOptions.SetPaperSizeByName("A4");
            if (TraySchemeFile != "")
            {
                //printJob.DocumentName = FileName;
                int count = 1;
                XmlBuilder xml = XmlBuilder.Builder();
                Pdf pdf = xml.DeserializeXml(TraySchemeFile);
                foreach (document t in pdf.document)
                {
                    foreach (SheetChange s in t.SheetChange)
                    {
                        PrintJobPage page = printJob.Pages[Convert.ToInt32(s.PageNumber) - 1];
                        page.PrintOptions.Inherit = false;
                        page.PrintOptions.PaperSource = printJob.Printer.PaperSources[Convert.ToInt32(_cassettoFromTipoCarta[s.SheetType])];
                        count++;
                    }
                }
            }
            printJob.Print();
            printJob.Dispose();
        }

        static void FaxSimple()
        {
            FaxPrintJob faxPrintJob = new FaxPrintJob("FaxPrinterName", "9,1-240-465-1177", Path.Combine(GetPath(), "LetterPortrait.pdf"));
            faxPrintJob.Print();
        }

        static void FaxSettingFaxOptions()
        {
            FaxPrintJob faxPrintJob = new FaxPrintJob("FaxPrinterName", "1-555-465-1177", Path.Combine(GetPath(), "LetterPortrait.pdf"));
            faxPrintJob.FaxOptions.RecipientName = "RecipientName";
            faxPrintJob.FaxOptions.SenderBillingCode = "SenderBillingCode";
            faxPrintJob.FaxOptions.SenderCompany = "SenderCompany";
            faxPrintJob.FaxOptions.SenderDept = "SenderDept";
            faxPrintJob.FaxOptions.SenderName = "SenderName";
            faxPrintJob.Print();
        }

        static void FaxWithCoverPage()
        {
            FaxPrintJob faxPrintJob = new FaxPrintJob("FaxPrinterName", "1-555-465-1177", Path.Combine(GetPath(), "LetterPortrait.pdf"));
            faxPrintJob.FaxOptions.CoverPage = new LocalCoverPage(Path.Combine(GetPath(), "Custom.cov")); // uses a local .cov file
            //faxPrintJob.FaxOptions.CoverPage = new ServerCoverPage("confdent");

            faxPrintJob.FaxOptions.CoverPage.Note = "Note";
            faxPrintJob.FaxOptions.CoverPage.RecipientCity = "RecipientCity";
            faxPrintJob.FaxOptions.CoverPage.RecipientCompany = "RecipientCompany";
            faxPrintJob.FaxOptions.CoverPage.RecipientCountry = "RecipientCountry";
            faxPrintJob.FaxOptions.CoverPage.RecipientDepartment = "RecipientDepartment";
            faxPrintJob.FaxOptions.CoverPage.RecipientFaxNumber = "RecipientFaxNumber";
            faxPrintJob.FaxOptions.CoverPage.RecipientHomePhone = "RecipientHomePhone";
            faxPrintJob.FaxOptions.CoverPage.RecipientName = "RecipientName";
            faxPrintJob.FaxOptions.CoverPage.RecipientOfficeLocation = "RecipientOfficeLocation";
            faxPrintJob.FaxOptions.CoverPage.RecipientOfficePhone = "RecipientOfficePhone";
            faxPrintJob.FaxOptions.CoverPage.RecipientState = "RecipientState";
            faxPrintJob.FaxOptions.CoverPage.RecipientStreetAddress = "RecipientStreetAddress";
            faxPrintJob.FaxOptions.CoverPage.RecipientTitle = "RecipientTitle";
            faxPrintJob.FaxOptions.CoverPage.RecipientZip = "RecipientZip";
            faxPrintJob.FaxOptions.CoverPage.SenderAddress = "SenderAddress";
            faxPrintJob.FaxOptions.CoverPage.SenderCompany = "SenderCompany";
            faxPrintJob.FaxOptions.CoverPage.SenderDepartment = "SenderDepartment";
            faxPrintJob.FaxOptions.CoverPage.SenderFaxNumber = "SenderFaxNumber";
            faxPrintJob.FaxOptions.CoverPage.SenderHomePhone = "SenderHomePhone";
            faxPrintJob.FaxOptions.CoverPage.SenderName = "SenderName";
            faxPrintJob.FaxOptions.CoverPage.SenderOfficeLocation = "SenderOfficeLocation";
            faxPrintJob.FaxOptions.CoverPage.SenderOfficePhone = "SenderOfficePhone";
            faxPrintJob.FaxOptions.CoverPage.SenderTitle = "SenderTitle";
            faxPrintJob.FaxOptions.CoverPage.Subject = "Subject";
            faxPrintJob.Print();
        }

        static void ShowPrintersOptions()
        {
            Printer printer = new Printer("PrinterName");
            //Or get from PrintJob.Printer property

            /*Console.WriteLine("Options for " + printer.Name);
            Console.WriteLine("  Collate: " + printer.Collate);
            Console.WriteLine("  Color: " + printer.Color);
            Console.WriteLine("  Duplex: " + printer.Duplex);
            Console.WriteLine("  MaxCopies: " + printer.MaxCopies);
            Console.WriteLine("  MaxPaperHeight: " + printer.MaxPaperHeight); // This should be changed to inches, points or mm
            Console.WriteLine("  MaxPaperWidth: " + printer.MaxPaperWidth); // This should be changed to inches, points or mm
            Console.WriteLine("  MinPaperHeight: " + printer.MinPaperHeight); // This should be changed to inches, points or mm
            Console.WriteLine("  MinPaperWidth: " + printer.MinPaperWidth); // This should be changed to inches, points or mm*/
            ListPaperSizes(printer.PaperSizes);
            ListPaperSources(printer.PaperSources);
            ListResolutions(printer.Resolutions);
        }
        // List Printer Options
        static void ListPaperSizes(PaperSizeList paperSizes)
        {
            Console.WriteLine("  List of Papers:");
            for (int i = 0; i < paperSizes.Count; i++)
            {
                Console.WriteLine("    " + paperSizes[i].Name + ": width=" + UnitConverter.PointsToInches(paperSizes[i].Width) + ", height=" + UnitConverter.PointsToInches(paperSizes[i].Height));
            }
        }

        static void ListPaperSources(PaperSourceList paperSources)
        {
            Console.WriteLine("  List of Paper Sources:");
            for (int i = 0; i < paperSources.Count; i++)
            {
                Console.WriteLine("    " + paperSources[i].Name);
            }
        }

        static void ListResolutions(ResolutionList resolutions)
        {
            Console.WriteLine("  List of Resolutions:");
            for (int i = 0; i < resolutions.Count; i++)
            {
                Console.WriteLine("    " + resolutions[i].HorizontalDpi + " dpi x " + resolutions[i].VerticalDpi + " dpi");
            }
        }

        static string GetPath()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory;
            if (path.EndsWith(new string(Path.DirectorySeparatorChar, 1)))
            {
                path = Path.GetDirectoryName(path);
            }
            while (Directory.Exists(path) && !Directory.Exists(Path.Combine(path, "PDFs")))
            {
                path = Path.GetDirectoryName(path);
            }
            if (path == null || !Directory.Exists(Path.Combine(path, "PDFs")))
            {
                throw new Exception("File path for pdf files could not be found.");
            }

            return path + "\\PDFs";
        }

      
    }
}
