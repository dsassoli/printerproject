﻿using L2C.Contracts.Interfaces;
using L2C.Contracts.Interfaces.Commands;
namespace L2C.Tasks.PdfPrinter
{
    class PdfPrinterStrategy : IStrategy
    {
        public PdfPrinterStrategy(string[] tokens)
        {
            Tokens = tokens;
        }

        public string[] Tokens { get; private set; }

        public IValidator Validator{get { return new PdfPrinterValidator(Tokens); }}

        public ICommand Command{get { return new PdfPrinterCommand(Tokens[1], Tokens[2], Tokens[3],Tokens[4]); }}
    }
}
