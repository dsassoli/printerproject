﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using iTextSharp.text.pdf;
using L2C.Contracts.Interfaces.Commands;
using MailMerge.Lib;
using Microsoft.Office.Interop.Word;
using DataTable = System.Data.DataTable;
using L2C.Tasks.PdfMerge;
using DLLSharedMethods;

namespace L2C.Tasks.MailMerge
{
    public class MailMergeCommand:IMailMergeCommand
    {
        #region varDeclarations
        public string DocTemplate { get; private set; }
        public string FileExcelInput { get; private set; }
        public string DestinationPdfFileName { get; private set; }
        public string DestinationXmlFileName { get; private set; }
        private string _tmpDirectory;
        private readonly string _connStr;
        private int _counter;
        private int _mergedFileCurrentPageNumber;
        private Pdf _pdf;
        private document _doc;
        private SheetChange _sheet;
        private XmlBuilder _xml;
        private Process _actual;
        DataTable _dt;
        #endregion

        public MailMergeCommand(string docTemplate,string fileExcelInput,string destinationPdfFileName,string destinationXmlFileName)
        {
            DocTemplate = docTemplate;
            FileExcelInput = fileExcelInput;
            DestinationPdfFileName = destinationPdfFileName;
            DestinationXmlFileName = destinationXmlFileName;
            _mergedFileCurrentPageNumber = 0;
            _connStr = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source='{0}';Extended Properties=Excel 12.0 Xml", fileExcelInput);
        }

        public void Execute()
        {
            _tmpDirectory = "C:\\L2Cmailmerge" + DateTime.Now.ToString("yyyyMMddHHmmssfff")+"\\";
            _pdf = Pdf.Builder();
            _xml = XmlBuilder.Builder();
            _pdf.document = new List<document>();
            //guardo i processi che ci sono prima di aprire quello attuale
            List<int> before = Process.GetProcessesByName("WINWORD").Select(d=>d.Id).ToList();
            Application myWordApp = new Application();
            //guardo i processi che ci sono dopo
            List<int> after = Process.GetProcessesByName("WINWORD").Select(d => d.Id).ToList();
            //identifico il processo che è stato appena aperto, come differenza che c'era fra quello prima e quello dopo.
            _actual = Process.GetProcessById(after.Except(before).First());
            //OBJECT OF MISSING "NULL VALUE" 
            object oMissing = System.Reflection.Missing.Value;
            //OBJECTS OF FALSE AND TRUE 
            object oTrue = true;
            object oFalse = false;
            //OBJECT OF DOCUMENT PATH
            try
            {
                object fileDocTempl = DocTemplate;
                Document myWordDoc = myWordApp.Documents.Add(ref fileDocTempl, ref oMissing, ref oTrue);
                Directory.CreateDirectory(_tmpDirectory);
                //*******************TROVARE NUMERO PAGINE FILE WORD*****************************
                //Word.WdStatistic stat = Word.WdStatistic.wdStatisticPages;
                //int num = myWordDoc.ComputeStatistics(stat, ref missing);
                myWordApp.Visible = false;
                Microsoft.Office.Interop.Word.MailMerge wrdMailMerge = myWordDoc.MailMerge;
                List<object> listTable = GetTableName();
                const string cmdText = "Select * from [{0}]";
                Object oQuery = string.Format(cmdText, listTable[0]);
                wrdMailMerge.OpenDataSource(FileExcelInput, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing,oMissing, oMissing, oMissing, oMissing, oMissing, oQuery);
                myWordDoc.MailMerge.Destination = Microsoft.Office.Interop.Word.WdMailMergeDestination.wdSendToNewDocument;
                //////// ************************************ IMPORTANTE ****************************
                myWordApp.MailMergeAfterRecordMerge += myWordApp_MailMergeAfterRecordMerge;
                //////// è più veloce mettere una pagina o segni salto pagina senza usare l'evento. Usare l'evento solo per salvare i singoli PDF
                myWordDoc.MailMerge.Execute(ref oFalse);
                _xml.SerializeXml(DestinationXmlFileName, _pdf);

                PdfMergeCommand pdfConcat = new PdfMergeCommand(_tmpDirectory, DestinationPdfFileName,1);
                pdfConcat.Execute();
                Directory.Delete(_tmpDirectory, true);
            }
            finally
            {
                _actual.Kill();
                //myWordDoc.Close(ref doNotSaveChanges, ref oMissing, ref oMissing);
                //myWordApp.Quit(ref doNotSaveChanges, ref oMissing, ref oMissing);
            }
        }
        #region otherFunctions
        private List<object> GetTableName()
        {
            _dt = GetSchema();
            var listTable = (from dr in _dt.AsEnumerable()
                             where dr["TABLE_NAME"].ToString().EndsWith("$")
                             select dr["TABLE_NAME"]).ToList();
            return listTable;
        }

        private DataTable GetSchema()
        {
            OleDbConnection conn = new OleDbConnection(_connStr);
            if (conn.State != ConnectionState.Open) conn.Open();
            DataTable dtSchema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
            if (conn.State == ConnectionState.Open) conn.Close();

            return dtSchema;
        }
        #endregion
        
        private void myWordApp_MailMergeAfterRecordMerge(_Document mergeResultDoc)
        {
            Object oMissing = System.Reflection.Missing.Value;
            _counter++;
            string filename = _counter.ToString(CultureInfo.InvariantCulture).PadLeft(8, '0');
            try
            {
                //inserisco le istruzioni nell'xml
                _doc = document.Builder();
                _doc.SheetChange = new List<SheetChange>();
                int numPagesInFile = 0;
                List<Bookmark> orderedResults = mergeResultDoc.Bookmarks.OfType<Bookmark>().OrderBy(d => d.Start).ToList();
                foreach (Bookmark bookMark in orderedResults)
                {//IMPORTANT: THE NAME OF THE BOOKMARK MUST BE CALLED WITH THE PAPER TYPE FOR THAT SECTION 
                    if (bookMark.Name == "ultimaPagina")
                        _doc.LastPage = Convert.ToString(_mergedFileCurrentPageNumber + numPagesInFile);
                    else
                    {
                        _sheet = SheetChange.Builder();
                        _sheet.PageNumber = Convert.ToString(Convert.ToInt32(bookMark.Range.Information[WdInformation.wdActiveEndPageNumber]) + _mergedFileCurrentPageNumber);
                        _sheet.SheetType = bookMark.Name;
                        _doc.SheetChange.Add(_sheet);
                        numPagesInFile++;
                    }
                }
                _doc.StartPage = Convert.ToString(_mergedFileCurrentPageNumber + 1);
                _mergedFileCurrentPageNumber += numPagesInFile;
                _pdf.document.Add(_doc);
                object destinationPdf = string.Format("{0}{1}.pdf", _tmpDirectory, filename);
                object wdSaveFormatPdf = WdSaveFormat.wdFormatPDF;
                mergeResultDoc.SaveAs(ref destinationPdf, ref wdSaveFormatPdf, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                                      ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                                      ref oMissing, ref oMissing, ref oMissing);
            }
            catch (Exception e)
            {
                LoggerHelper.Log(DateTime.Now + "error: " +e.StackTrace);
                object doNotSaveChanges = WdSaveOptions.wdDoNotSaveChanges;
                mergeResultDoc.Close(ref doNotSaveChanges, ref oMissing, ref oMissing);
            }
        }
    }
}
