﻿using L2C.Contracts.Interfaces;
using L2C.Contracts.Interfaces.Commands;
namespace L2C.Tasks.MailMerge
{
    public class MailMergeStrategy : IStrategy
    {
        public MailMergeStrategy(string[] tokens)
        {
            Tokens = tokens;
        }

        public string[] Tokens { get; private set; }
        public IValidator Validator { get { return new MailMergeValidator(Tokens); } }
        public ICommand Command { get { return new MailMergeCommand(Tokens[1], Tokens[2], Tokens[3], Tokens[4]); } }
    }
}