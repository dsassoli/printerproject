﻿using System.IO;
using L2C.Contracts.Interfaces;

namespace L2C.Tasks.MailMerge
{
    public class MailMergeValidator : IValidator
    {
        private readonly string[] _tokens;

        public MailMergeValidator(string[] tokens)
        {
            _tokens = tokens;
        }
        public bool IsValid()
        {
            if ((Path.GetExtension(_tokens[1]) == ".doc" || Path.GetExtension(_tokens[1]) == ".docx") && (Path.GetExtension(_tokens[2]) == ".xls" || Path.GetExtension(_tokens[2]) == ".xlsx") && (Path.GetExtension(_tokens[3]) == ".pdf") && (Path.GetExtension(_tokens[4]) == ".xml"))
                return true;
            else
                return false;
        }
    }
}