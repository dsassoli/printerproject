﻿using System;
using System.Configuration;
using System.IO;
using System.Windows.Forms;

namespace DLLSharedMethods
{
    public static class LoggerHelper
    {
        private static string _path;

        public static void Log(string message, bool success)
        {
            CreateLog();
            if (success)
            {
                try
                {
                    using (var sw = File.AppendText(_path))
                    {
                        sw.WriteLine("**nuovo errore**");
                        sw.WriteLine();
                        sw.WriteLine(message + "\r\n");
                        sw.WriteLine("***********");
                    }
                }
                catch
                {
                }            
            }
        }

        public static void Log(string message)
        {
            CreateLog();
            try
            {
                using (var sw = File.AppendText(_path))
                {
                    sw.WriteLine("**nuovo errore**");
                    sw.WriteLine();
                    sw.WriteLine(message + "\r\n");
                    sw.WriteLine("***********");
                }
            }
            catch
            {
            }
        }

        public static void Log(Exception exc)
        {
            CreateLog();
            try
            {
                using (StreamWriter sw = new StreamWriter(_path, true))
                {
                    sw.Write("******************** " + DateTime.Now);
                    sw.WriteLine("********************");
                    if (exc.InnerException != null)
                    {
                        sw.Write("Inner Exception Type: ");
                        sw.WriteLine(exc.InnerException.GetType().ToString());
                        sw.Write("Inner Exception: ");
                        sw.WriteLine(exc.InnerException.Message);
                        sw.Write("Inner Source: ");
                        sw.WriteLine(exc.InnerException.Source);
                        if (exc.InnerException.StackTrace != null)
                            sw.WriteLine("Inner Stack Trace: ");
                        sw.WriteLine(exc.InnerException.StackTrace);
                    }
                    sw.Write("Exception Type: ");
                    sw.WriteLine(exc.GetType().ToString());
                    sw.WriteLine("Exception: " + exc.Message);
                    sw.WriteLine("Stack Trace: ");
                    if (exc.StackTrace != null)
                        sw.WriteLine(exc.StackTrace);
                    sw.WriteLine();
                    sw.WriteLine("*************************************************************");
                }
            }
            catch
            {
            }
        }

        public static void CreateLog()
        {
            _path = Path.Combine(ConfigurationManager.AppSettings["currentPath"], "log.txt");
            if(!File.Exists(_path))
                File.Create(_path).Close();
        }

        // Log an Exception
        public static void Log(Exception exc, string source)
        {
            CreateLog();
            using (StreamWriter sw = new StreamWriter(_path, true))
            {
                sw.Write("******************** " + DateTime.Now);
                sw.WriteLine("********************");
                if (exc.InnerException != null)
                {
                    sw.Write("Inner Exception Type: ");
                    sw.WriteLine(exc.InnerException.GetType().ToString());
                    sw.Write("Inner Exception: ");
                    sw.WriteLine(exc.InnerException.Message);
                    sw.Write("Inner Source: ");
                    sw.WriteLine(exc.InnerException.Source);
                    if (exc.InnerException.StackTrace != null)
                        sw.WriteLine("Inner Stack Trace: ");
                    sw.WriteLine(exc.InnerException.StackTrace);
                }
                sw.Write("Exception Type: ");
                sw.WriteLine(exc.GetType().ToString());
                sw.WriteLine("Exception: " + exc.Message);
                sw.WriteLine("Source: " + source);
                sw.WriteLine("Stack Trace: ");
                if (exc.StackTrace != null)
                    sw.WriteLine(exc.StackTrace);
                sw.WriteLine();
                sw.WriteLine("*************************************************************");
                
            }
        }
    }
}
