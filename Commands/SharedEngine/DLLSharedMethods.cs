using System;
using System.Collections.Specialized;
using System.Collections;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using iTextSharp.text;
using iTextSharp.text.pdf;
using IEC16022Sharp;

/*
 * V. 0.1 alpha - 30,31/08/2007
 * 
 * Descrizione:
 * Questa DLL contiene funzioni condivise dalle altre procedure di elaborazione pdf.
 * E' una classe statica in quanto contiene solo metodi.
 */ 

namespace DLLSharedMethods
{
    public static class SharedMethods
    {
        #region Variabili private e costanti

        /// <summary>
        /// Il numero massimo di byte in una sezione del file ini.
        /// </summary>
        private const int MAX_ENTRY = 32768;

        private const float INDIRIZZO_Y = 648F;
        private const float INDIRIZZO_X = 291F;

        private static BaseFont _courier = BaseFont.CreateFont(GetFontFolderPath() + BaseFont.COURIER + "o.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);

        //private readonly static short _Salvataggio = Convert.ToInt16(PDSaveFlags.PDSaveFull | PDSaveFlags.PDSaveCollectGarbage);

        public struct DataMatrixPoste
        {
            public string Identificatore;
            public string Disponibile1;
            public string IdClienteSAP;
            public string ContoContrattuale;
            public string ClasseProdotto;
            public string TipoTariffa;
            public string CAP_Destinatario;
            public string CodTecnico_Destinatario;
            public string CAP_Mittente;
            public string CodTecnico_Mittente;
            public string Identificativo_ClienteMittente;
            public string Disponibile2;
            public string Causale;
            public string CodOmologazione;
            public string Disponibile3;

            /// <summary>
            /// Inizializza le variabili con il carattere indicato
            /// </summary>
            /// <param name="_c">carattere</param>
            public DataMatrixPoste(char _c)
            {
                 Identificatore = new string(_c, 1);
                 Disponibile1 = new string(_c, 1);
                 IdClienteSAP = new string(_c, 8);
                 ContoContrattuale = new string(_c, 3);
                 ClasseProdotto = new string(_c, 1);
                 TipoTariffa = new string(_c, 1);
                 CAP_Destinatario = new string(_c, 5);
                 CodTecnico_Destinatario = new string(_c, 4);
                 CAP_Mittente = new string(_c, 5);
                 CodTecnico_Mittente = new string(_c, 4);
                 Identificativo_ClienteMittente = new string(_c, 3);
                 Disponibile2 = new string(_c, 10);
                 Causale = new string(_c, 3);
                 CodOmologazione = new string(_c, 6);
                 Disponibile3 = new string(_c, 17);
            }
        }

        /// <summary>
        /// Dati necessari per il barcode sulle buste TNT
        /// </summary>
        public struct C128TNT
        {
            public string SiglaAzienda;
            public string NumeroLotto;
            public string ProgressivoBusta;
            public Posizione PosizioneBarCode;
            public enum Posizione { Indirizzo, Raccomandata };

            public C128TNT(char _c)
            {
                SiglaAzienda = new string(_c, 3);
                NumeroLotto = new string(_c, 6);
                ProgressivoBusta = new string(_c, 8);
                PosizioneBarCode = Posizione.Indirizzo;
            }
        }

        /// <summary>
        /// Dati necessari per ogni datamatrix per ogni pagina
        /// </summary>
        public struct DataMatrixImbustamento
        {
            public string JobId;                // 8 -> id del job
            public string ProgressivoBusta;     // 6 -> progressivo della busta rispetto al job
            public string ProgressivoPagina;    // 2 -> progressivo del foglio rispetto al totale della busta
            public string TotaleFogliBusta;     // 2 -> il totale dei fogli della busta
            public string Accessori;            // 3 -> 0/1 corrispondenti agli inserti
            public float X;
            public float Y;

            public DataMatrixImbustamento(char _c, float _X, float _Y)
            {
                JobId = new string(_c, 8);
                ProgressivoBusta = new string(_c, 6);
                ProgressivoPagina = new string(_c, 2);
                TotaleFogliBusta = new string(_c, 2);
                Accessori = new string(_c, 3);
                if (_X == 0)
                    X = 10F;
                else
                    X = _X;
                if (_Y == 0)
                    Y = 740F;
                else
                    Y = _Y;
            }
        }

        public struct DataMatrixImbustamentoBB600
        {
            public string JobId;                // 8 -> id del job
            public string ProgressivoBusta;     // 6 -> progressivo della busta rispetto al job
            public string ProgressivoPagina;    // 2 -> progressivo del foglio rispetto al totale della busta
            public string TotaleFogliBusta;     // 2 -> il totale dei fogli della busta
            public string Accessori;            // 4 -> 0/1 corrispondenti agli inserti
            public float X;
            public float Y;
            public string ChiudiBusta;          // 1 -> 0/1 solo per C6
            public string ChiamataSlave;        // 1 -> 0/1 terminato il master passa allo slave
            public string KickerScatola;        // 1 -> 0/1 solo per C6 fine scatola, sfalza sul tappeto
            public string KickerTappeto;        // 1 -> 0/1 se busta > C6

            public DataMatrixImbustamentoBB600(char _c, float _X, float _Y)
            {
                JobId = new string(_c, 8);
                ProgressivoBusta = new string(_c, 6);
                ProgressivoPagina = new string(_c, 2);
                TotaleFogliBusta = new string(_c, 2);
                Accessori = new string(_c, 4);
                ChiudiBusta = "0";
                ChiamataSlave = "0";
                KickerScatola = "0";
                KickerTappeto = "0";
                if (_X == 0)
                    X = 10F;
                else
                    X = _X;
                if (_Y == 0)
                    Y = 40F;
                else
                    Y = _Y;
            }
        }

        #endregion

        #region Dichiarazione API
        /// <summary>
        /// GetPrivateProfileString estrae una stringa da una specifica sezione di un file INI.
        /// </summary>
        [DllImport("KERNEL32.DLL", EntryPoint = "GetPrivateProfileStringA", CharSet=CharSet.Ansi)]
        private static extern int GetPrivateProfileString (string lpApplicationName,
                                                           string lpKeyName, 
                                                           string lpDefault, 
                                                           StringBuilder lpReturnedString, 
                                                           int nSize,
                                                           string lpFileName);

        [DllImport("shell32.dll")]
        private static extern int SHGetFolderPath(IntPtr hwndOwner, int nFolder, IntPtr hToken,
                   uint dwFlags, [Out] StringBuilder pszPath);

        public static string GetFontFolderPath()
        {
            StringBuilder sb = new StringBuilder();
            SHGetFolderPath(IntPtr.Zero, 0x0014, IntPtr.Zero, 0x0000, sb);

            return sb.ToString() + @"\";
        }

        /// <summary>
        /// GetPrivateProfileSection estrae le chiavi da una specifica sezione di un file INI.
        /// </summary>
        [DllImport("KERNEL32.DLL", EntryPoint = "GetPrivateProfileSectionW", CharSet = CharSet.Unicode)]
        private static extern int GetPrivateProfileSection(string lpApplicationName, StringBuilder lpReturnedString, int nSize, string lpFileName);

        /// <summary>
        /// GetPrivateProfileSectionNames estrae le sezioni di un file INI.
        /// Sembra non funzionare, estrae solo la prima sezione che trova.
        /// </summary>
        [DllImport("KERNEL32.DLL", EntryPoint = "GetPrivateProfileSectionNamesW", CharSet = CharSet.Unicode)]
        private static extern int GetPrivateProfileSectionNames(StringBuilder lpszReturnBuffer, int nSize, string lpFileName);

        #endregion


    
        public static int FaiBinder(string _FilePdfPadre, ref PdfReader _FileDaUnire, int _DaPagina, int _NumeroDiPagine, int _AchePagina, int _QuantePagine)
        {
           
            try
            {
                Document document = null;
                PdfCopy writer = null;
                //PdfReader reader = null;
                PdfImportedPage page;
                PRAcroForm form = null;

                int n = 0;

                // we create a reader for a certain document
                if (File.Exists(_FilePdfPadre))
                {
                    PdfReader reader = null;
                    reader = new PdfReader(_FilePdfPadre);
                    // we retrieve the total number of pages
                    n = reader.NumberOfPages;

                    // step 1: creation of a document-object
                    document = new Document(reader.GetPageSizeWithRotation(1));
                    // step 2: we create a writer that listens to the document                                                                        
                    Stream _s = new FileStream(_FilePdfPadre + ".new", FileMode.Create);

                    writer = new PdfCopy(document, _s);
                    writer.PDFXConformance = PdfWriter.PDFA1B;
                    writer.CreateXmpMetadata();
                    // non mette bene i riferimenti ai font embedded
                    //writer = new PdfSmartCopy(document, _s);                        

                    // step 3: we open the document
                    document.Open();

                    // step 4: we add content                  
                    for (int i = 0; i < n; )
                    {
                        ++i;
                        page = writer.GetImportedPage(reader, i);
                        writer.AddPage(page);

                        if (i == _AchePagina)
                        {
                            // step 4: we add content
                            for (int i2 = _DaPagina; i2 < (_DaPagina + _NumeroDiPagine); i2++)
                            {
                                PdfImportedPage page2 = writer.GetImportedPage(_FileDaUnire, i2);

                                for (int m = 0; m < _QuantePagine; m++)
                                {
                                    writer.AddPage(page2);
                                }                                
                            }
                        }
                    }

                    form = reader.AcroForm;
                    if (form != null)
                        writer.CopyAcroForm(reader);

                    reader.Close();
                }
                else
                {
                    document = new Document();

                    // step 2: we create a writer that listens to the document                                                                        
                    Stream _s = new FileStream(_FilePdfPadre + ".new", FileMode.Create);

                    writer = new PdfCopy(document, _s);
                    writer.PDFXConformance = PdfWriter.PDFA1B;
                    writer.CreateXmpMetadata();
                    // non mette bene i riferimenti ai font embedded
                    //writer = new PdfSmartCopy(document, _s);                        

                    // step 3: we open the document
                    document.Open();
                    // we create a reader for a certain document
                    //reader = new PdfReader(_FileDaUnire);
                    // we retrieve the total number of pages
                    n = _FileDaUnire.NumberOfPages;

                    // step 4: we add content
                    for (int i = _DaPagina; i < (_DaPagina + _NumeroDiPagine); i++)
                    {
                        page = writer.GetImportedPage(_FileDaUnire, i);
                        writer.AddPage(page);
                    }
                }


                /*form = _FileDaUnire.AcroForm;
                if (form != null)
                    writer.CopyAcroForm(_FileDaUnire);*/

                // step 5: we close the document
                try
                {
                    writer.Close();

                    //Marshal.FinalReleaseComObject(writer);
                    writer = null;
            
                    document.Close();

                    //Marshal.FinalReleaseComObject(document);
                    document = null;
                }
                catch (Exception ex)
                {
                    LoggerHelper.Log(ex.StackTrace);
                    throw ex;
                }

                try
                {
                    File.Delete(_FilePdfPadre);
                    File.Move(_FilePdfPadre + ".new", _FilePdfPadre);
                }
                catch (Exception ex)
                {
                    LoggerHelper.Log(ex.StackTrace);
                    throw ex;
                }
                
               

                /***********************************/
                /* vecchia procedura con acrobat */
                /*if (System.IO.File.Exists(_FilePdfPadre))
                {
                    _PdfPadre.Open(_FilePdfPadre);
                }
                else
                {
                    _PdfPadre.Create();
                }

                _PdfDaAgg.Open(_FileDaUnire);

                _PdfPadre.InsertPages(_PdfPadre.GetNumPages() - 1, _PdfDaAgg, _DaPagina - 1, _NumeroDiPagine, 0);
                _Ritorno[1] = _NumeroDiPagine.ToString();

                _PdfDaAgg.Close();
                //_PdfPadre.Save(PDSaveFlags.PDSaveIncremental, _FileDaUnire[0]);
                _PdfPadre.Save(_Salvataggio, _FilePdfPadre);*/
                /************************************/
            }
            catch (Exception ex)
            {
                LoggerHelper.Log(ex.StackTrace);
                throw ex;
            }
             return _NumeroDiPagine;
        }


        /// <summary>
        /// Utilizza classi java per concatenare i pdf
        /// </summary>
        /// <param name="_ListaFile">Array con la lista dei file da concatenare</param>
        /// <param name="_FileFinale">File finale</param>
        /// <returns></returns>
        public static int FaiBinder(string[] _ListaFile, string _FileFinale)
        {
            int _NumeroFaccInserite = 0;
            try 
            {
                int f = 0;
                

                //Document document = null;
                //PdfCopy  writer = null;
                using (MemoryStream ms = new MemoryStream())
                {
                    PdfCopyFields writer2 = new PdfCopyFields(ms);
                    writer2.Writer.PDFXConformance = PdfWriter.PDFA1B;
                    writer2.Writer.CreateXmpMetadata();

                    while (f < _ListaFile.Length)
                    {
                        // we create a reader for a certain document
                        PdfReader reader = new PdfReader(_ListaFile[f]);
                        // we retrieve the total number of pages
                        int n = reader.NumberOfPages;
                        _NumeroFaccInserite += n;

                        /*if (f == 0)
                        {
                            // step 1: creation of a document-object
                            document = new Document(reader.GetPageSizeWithRotation(1));
                            // step 2: we create a writer that listens to the document                                                                        
                            Stream _s = new FileStream(_FileFinale, FileMode.Create);

                            writer = new PdfCopy(document, _s);
                            writer.PDFXConformance = PdfWriter.PDFA1B;
                            writer.CreateXmpMetadata();

                            

                            // non mette bene i riferimenti ai font embedded
                            writer = new PdfSmartCopy(document, _s);                        

                            // step 3: we open the document
                            document.Open();
                        }*/

                        // step 4: we add content
                        /*PdfImportedPage page;
                        for (int i = 1; i <= n; i++) {
                            page = writer.GetImportedPage(reader, i);                        
                            writer.AddPage(page);
                        }
                    
                        PRAcroForm form = reader.AcroForm;
                        if (form != null)
                            writer.CopyAcroForm(reader);*/

                        writer2.AddDocument(reader);

                        f++;

                        reader.Close();
                        reader = null;
                    }

                    writer2.Close();
                    MemoryStreamToFile(ms, _FileFinale);
                }
            }
            catch(Exception ex) 
            {
                LoggerHelper.Log(ex.StackTrace);
                throw ex;
            }
            return _NumeroFaccInserite;
           
        }

        public static int FaiBinder(string[] _ListaFile, ref byte[] _FileFinale)
        {
            int _NumeroFaccInserite = 0;
            try
            {
                int f = 0;
                

                //Document document = null;
                //PdfCopy  writer = null;
                using (MemoryStream ms = new MemoryStream())
                {
                    PdfCopyFields writer2 = new PdfCopyFields(ms);
                    writer2.Writer.PDFXConformance = PdfWriter.PDFA1B;
                    writer2.Writer.CreateXmpMetadata();
                    PdfReader reader;

                    while (f < _ListaFile.Length)
                    {
                        byte[] _b = File.ReadAllBytes(_ListaFile[f]);
                        // we create a reader for a certain document
                        reader = new PdfReader(_b);
                        // we retrieve the total number of pages
                        int n = reader.NumberOfPages;
                        _NumeroFaccInserite += n;

                        
                        writer2.AddDocument(reader);

                        f++;
                        writer2.Writer.FreeReader(reader);
                        
                        reader.Close();
                        reader = null;
                        
                        writer2.Writer.Flush();

                    }

                    writer2.Close();
                    _FileFinale = ms.ToArray();
                }

                GC.Collect();
                
                // step 5: we close the document
                /*try { 
                    writer.Close();
                    
                    //Marshal.FinalReleaseComObject(writer);
                    writer = null;
                }
                catch { }*/

                /*try { 
                    document.Close();
                    
                    //Marshal.FinalReleaseComObject(document);
                    document = null;
                }
                catch { }*/

                return _NumeroFaccInserite;
            }
            catch (Exception ex)
            {
                LoggerHelper.Log(ex.StackTrace);
                throw ex;
            }

           
        }

        public static int FaiBinderEruotaTemplate(PdfReader readerFileFinale, PdfReader readerTemplate, ref string _FilePdfTemp, int ultimaFacciata)
        {
            
            int numeroFaccInserite = 0;

            try
            {

               
                using (MemoryStream ms = new MemoryStream())
                {
                    //StreamWriter _pdftemp = new StreamWriter(_FilePdfTemp, false);
                    PdfStamper stamp = new PdfStamper(readerFileFinale, ms);

                    /******************************************************/
                    // MODIFICA 17-20 OTTOBRE 2011 PER CREAZIONE PDF IN MODALITA' PDF/A
                    stamp.Writer.PDFXConformance = PdfWriter.PDFA1B;
                    stamp.Writer.CreateXmpMetadata();


                    PdfContentByte _wri = null; // stamp2.GetUnderContent(_ultimafacciata);


                    //******************************************************
                    //Modifica Simone solobollettini 07-03-2013
                    //Ruota il file template del bollettino e lo mette sotto
                    //il documento in elaborazione



                    for (int i = 1; i <= readerTemplate.NumberOfPages; i++)
                    {

                        PdfImportedPage page = stamp.GetImportedPage(readerTemplate, i);

                        if (i == 1)
                        {                            

                            _wri = stamp.GetUnderContent(ultimaFacciata);

                            int rotazione = readerTemplate.GetPageSizeWithRotation(i).Rotation;

                            if (rotazione == 0)
                            {
                                float rotate = 270;
                                float x = readerTemplate.GetPageSize(i).Width;
                                float y = readerTemplate.GetPageSize(i).Height;
                                float angle = (float)(-rotate * (Math.PI / 180));
                                float xRot = (float)Math.Cos(angle);
                                float yRot = (float)Math.Cos(angle);
                                float xScale = (float)-Math.Sin(angle);
                                float yScale = (float)-Math.Sin(angle);

                                _wri.AddTemplate(page, xScale, xRot, yRot, yScale, x, y);

                                //  _wri.AddTemplate(page, -1f, 0, 0, -1f,
                                //reader.GetPageSizeWithRotation(i).Width,
                                //reader.GetPageSizeWithRotation(i).Height);

                            }
                            else if (rotazione == 90 || rotazione == 270)
                            {
                                float rotate = 90;
                                float x = readerTemplate.GetPageSize(i).Height;
                                float y = 0;
                                float angle = (float)(-rotate * (Math.PI / 180));
                                float xScale = (float)Math.Cos(angle);
                                float yScale = (float)Math.Cos(angle);
                                float xRot = (float)-Math.Sin(angle);
                                float yRot = (float)Math.Sin(angle);

                                _wri.AddTemplate(page, xScale, xRot, yRot, yScale, x, y);

                            }
                            else if (rotazione == 180)
                            {
                                _wri.AddTemplate(page, 0f, 0f);
                            }

                        }
                        else if (i == 2)
                        {                            

                            _wri = stamp.GetUnderContent(ultimaFacciata + 1);

                            int rotazione = readerTemplate.GetPageSizeWithRotation(i).Rotation;

                            if (rotazione == 0)
                            {
                                float rotate = -270;
                                float x = readerTemplate.GetPageSize(i).Height;
                                float y = readerTemplate.GetPageSize(i).Width;
                                float angle = (float)(-rotate * (Math.PI / 180));
                                float xRot = (float)Math.Cos(angle);
                                float yRot = (float)Math.Cos(angle);
                                float xScale = (float)-Math.Sin(angle);
                                float yScale = (float)-Math.Sin(angle);

                                _wri.AddTemplate(page, xScale, xRot, yRot, yScale, x, y);

                                //  _wri.AddTemplate(page, -1f, 0, 0, -1f,
                                //reader.GetPageSizeWithRotation(i).Width,
                                //reader.GetPageSizeWithRotation(i).Height);

                            }
                            else if (rotazione == 90 || rotazione == 270)
                            {
                                float rotate = -90;
                                float x = 0;
                                float y = page.Width;
                                float angle = (float)(-rotate * (Math.PI / 180));
                                float xScale = (float)Math.Cos(angle);
                                float yScale = (float)Math.Cos(angle);
                                float xRot = (float)-Math.Sin(angle);
                                float yRot = (float)Math.Sin(angle);


                                _wri.AddTemplate(page, xScale, xRot, yRot, yScale, x, y);
                            }
                            else if (rotazione == 180)
                            {
                                _wri.AddTemplate(page, 0f, 0f);
                            }
                        }
                    }


                    int n = readerFileFinale.NumberOfPages;
                    numeroFaccInserite += n;

                    stamp.Close();
                    MemoryStreamToFile(ms, _FilePdfTemp);

                }


                GC.Collect();
                return numeroFaccInserite;
             }
            catch (Exception ex)
            {
                LoggerHelper.Log(ex.StackTrace);
                throw ex;
            }

         

        }

        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException ioex)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)

                LoggerHelper.Log(ioex.StackTrace);
                throw ioex;
                
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        #region MemoryStreamToFile
        /// <summary>
        /// Saves a MemoryStream to the specified file name
        /// </summary>
        /// <param name="MS">MemoryStream to save</param>
        /// <param name="FileName">File name to save MemoryStream as</param>
        public static void MemoryStreamToFile(MemoryStream MS, string FileName)
        {
            using (FileStream fs = new FileStream(@FileName, FileMode.Create, FileAccess.Write, FileShare.None, 8, false))
            {
                try
                {
                    byte[] data = MS.ToArray();
                    fs.Write(data, 0, data.Length);
                }
                catch(Exception ex)
                {
                    LoggerHelper.Log(ex.StackTrace);
                    throw ex;
                }
                finally
                {
                    fs.Close();
                    GC.Collect();
                }
            }
           
        }
        #endregion
     


        /*
         * Funzione per scrivere pi� righe di testo in un contenuto pdf
         */
        public static void _ScriviRighe(string[] _Righe, ref PdfContentByte _wri, float _X, float _Y, int _rotazione, float _BOLLINTERLINEA)
        {
            int _IndiceRiga = 0;

            foreach (string _Riga in _Righe)
            {
                switch (_rotazione)
                {
                    case 90:
                        _wri.SetTextMatrix(_X - (_BOLLINTERLINEA * _IndiceRiga), _Y);
                        break;

                    default:
                        _wri.SetTextMatrix(_X, _Y - (_BOLLINTERLINEA * _IndiceRiga));
                        break;
                }

                _wri.ShowText(_Riga);
                _IndiceRiga++;
            }
        }

        public static void _ScriviRighe(string[] _Righe, ref PdfContentByte _wri, float _X, float _Y, int _rotazione, float _A, float _B, float _C, float _D, float _BOLLINTERLINEA)
        {
            int _IndiceRiga = 0;

            foreach (string _Riga in _Righe)
            {
                switch (_rotazione)
                {
                    case 270:
                        // ricalcolo le misure rispetto alla nuova Y
                        // 840 72esimi di pollice � la lunghezza della carta A4
                        _Y = 840 - _Y;
                        _wri.SetTextMatrix(_A, _B, _C, _D, _X - (_BOLLINTERLINEA * _IndiceRiga), _Y);
                        break;

                    case 90:
                        // ricalcolo le misure rispetto alla nuova X
                        // x = 594 72esimi di pollice � l'altezza della carta A4
                        _X = 596 - _X;
                        _wri.SetTextMatrix(_A, _B, _C, _D, _X + (_BOLLINTERLINEA * _IndiceRiga), _Y);
                        break;

                    default:
                        _wri.SetTextMatrix(_A, _B, _C, _D, _X, _Y - (_BOLLINTERLINEA * _IndiceRiga));
                        break;
                }

                _wri.ShowText(_Riga);
                _IndiceRiga++;
            }
        }

          

        private static int _MettiSegniWriter(ref PdfReader reader, ref PdfContentByte _wri, int _contapag, ref int _InizContaPagina, ArrayList ChiudiBusta, float[] _SEGNI, float _MARGINEDX, string _IMGSIGNRESPECT, float _LARGHEZZAZONASEGNI, float _MARGINEZONASEGNI, float _DISTANZASEGNI, int _SpessoreSegni, ref int _InizContatoreBinario, bool _FR, ref int _InizContaBusta, string _CN, bool _InserisciDtmxImbustamento, bool _InserisciSegniImbustamento, ref ArrayList _Clear, ref Hashtable _Inserti, ref Hashtable _Fogli, ref NameValueCollection _FileSum, ref ArrayList _PagineSegnoTaglierina)
        {

            int _RitornoMessaggio = 0;

            try
            {

                BaseFont _fontsegno = BaseFont.CreateFont(GetFontFolderPath() + "timr45w.ttf", BaseFont.WINANSI, true);


                /* resetto il contatore binario se < 1 */
                // il contatore parte da 7 e va a scendere
                if (_InizContatoreBinario < 1)
                    _InizContatoreBinario = 7;

                DataMatrixImbustamento _dmtx = new DataMatrixImbustamento();
                DataMatrixImbustamentoBB600 _dmtxbb600 = new DataMatrixImbustamentoBB600();

                if (_FileSum["tipo_segni"] == "8")
                {
                    _dmtxbb600 = new DataMatrixImbustamentoBB600('0', 9, 69);
                    _dmtxbb600.JobId = _CN;
                    _dmtxbb600.ProgressivoBusta = _InizContaBusta.ToString().PadLeft(6, '0');
                    _dmtxbb600.ProgressivoPagina = _InizContaPagina.ToString().PadLeft(2, '0');
                    if (_Inserti.Count > 0)
                    {
                        if (_Inserti.ContainsKey(_contapag))
                        {
                            string _nuoviacc = "";
                            for (byte _ins = 1; _ins <= 4; _ins++)
                            {
                                if (_Inserti[_contapag.ToString()].ToString().IndexOf(_ins.ToString()) > -1)
                                {
                                    _nuoviacc += "1";
                                }
                                else
                                    _nuoviacc += "0";
                            }
                            _dmtxbb600.Accessori = _nuoviacc;
                        }
                    }
                    _dmtxbb600.TotaleFogliBusta = _Fogli[_InizContaBusta].ToString().PadLeft(2, '0');
                    if (ChiudiBusta.Contains(_contapag))
                        _dmtxbb600.ChiudiBusta = "1";
                    else
                        _dmtxbb600.ChiudiBusta = "0";
                    _dmtxbb600.ChiamataSlave = "0";
                    _dmtxbb600.KickerScatola = "0";
                    _dmtxbb600.KickerTappeto = "0";
                }
                else
                {

                    if (_FileSum["codcliente"] == "unico")
                    {
                        _dmtx = new DataMatrixImbustamento('0', 6, 690);
                    }
                    else
                        _dmtx = new DataMatrixImbustamento('0', 0, 0);

                    _dmtx.JobId = _CN;
                    _dmtx.ProgressivoBusta = _InizContaBusta.ToString().PadLeft(6, '0');
                    _dmtx.ProgressivoPagina = _InizContaPagina.ToString().PadLeft(2, '0');
                    _dmtx.TotaleFogliBusta = _Fogli[_InizContaBusta].ToString().PadLeft(2, '0');

                    if (_Inserti.Count > 0)
                    {
                        if (_Inserti.ContainsKey(_contapag))
                        {
                            string _nuoviacc = "";
                            for (byte _ins = 1; _ins <= 3; _ins++)
                            {
                                if (_Inserti[_contapag.ToString()].ToString().IndexOf(_ins.ToString()) > -1)
                                {
                                    _nuoviacc += "1";
                                }
                                else
                                    _nuoviacc += "0";
                            }
                            _dmtx.Accessori = _nuoviacc;
                        }
                    }
                }
                
                Rectangle _pagina = reader.GetPageSize(_contapag);

                Rectangle _rotazioneret = reader.GetPageSizeWithRotation(_contapag);
                int _rotazione = _rotazioneret.Rotation;
                // metto i segni comunque nella posizione fissata,
                // indipendentemente dalla rotazione della pagina
                //int _rotazione = 0;
                int _contasegni = 0;

                /*
                    * Attenzione, a video i segni non sono al margine della pagina
                    * perch� la stampante ha dei margini in cui non stampa
                    * Settaggi per avere i segni al margine nel pdf:
                // altezza
                float _segnoy = _pagina.height() - 20;
                // larghezza
                float _segnox = _pagina.width() - 6;
                */

                // altezza
                float _segnoy = _SEGNI[0];

                // larghezza
                float _segnox = _pagina.Width - _MARGINEDX;

                if (_rotazione == 270 || _rotazioneret.Width > _rotazioneret.Height)
                    _segnox = PageSize.A4.Width - _MARGINEDX;

                /*
                    * Mette il bianco nella zona segni
                    */
                if (_InserisciSegniImbustamento == true)
                    _RitornoMessaggio = _MettiImg(_IMGSIGNRESPECT, _segnox - _LARGHEZZAZONASEGNI, _SEGNI[5] - _MARGINEZONASEGNI, _LARGHEZZAZONASEGNI + _MARGINEDX, (_DISTANZASEGNI * 6) + (_MARGINEZONASEGNI * 2), ref _wri);
                if (_RitornoMessaggio <= 0)
                    return _RitornoMessaggio;

                if (_InizContaPagina < 1000)
                {
                    if (_InserisciDtmxImbustamento && _FileSum["tipo_segni"] != "8")
                        _RitornoMessaggio = _InserisciDataMatrix(_dmtx, ref _wri, _dmtx.X, _dmtx.Y, _IMGSIGNRESPECT);
                    else if (_InserisciDtmxImbustamento && _FileSum["tipo_segni"] == "8")
                    {
                        // il segno va sul retro, i lotti sono sempre F/R per questo tipo di segni
                        //PdfContentByte _wriRetro = stamp2.GetOverContent(_contapag + 1);
                        //_RitornoMessaggio = _InserisciDataMatrixBB600(_dmtxbb600, ref _wriRetro, _dmtxbb600.X, _dmtxbb600.Y, _IMGSIGNRESPECT);
                    }

                    if (_RitornoMessaggio <= 0)
                        return _RitornoMessaggio;
                }


                if (_PagineSegnoTaglierina.Contains(_contapag))
                {
                    _RitornoMessaggio = _InserisciSegnoTaglio(ref _wri);

                    if (_RitornoMessaggio <= 0)
                        return _RitornoMessaggio;

                    // commentato perch� qui non ho lo stamper
                    //if (_FR)
                    //{
                    //    PdfContentByte _wriretro = stamp2.GetOverContent(_contapag + 1);
                    //    _RitornoMessaggio = _InserisciSegnoTaglio(ref _wriretro);

                    //    if (_RitornoMessaggio[0] != "O")
                    //        return _RitornoMessaggio;
                    //}
                }

                _wri.BeginText();
                _wri.SetFontAndSize(_fontsegno, 30);

                /*
                    * Start
                    */
                if (_InserisciSegniImbustamento == true)
                    _RitornoMessaggio = _ScriviSegno(_segnox, _segnoy, _rotazione, ref _wri, _SpessoreSegni);
                if (_RitornoMessaggio <= 0)
                    return _RitornoMessaggio;

                _contasegni++;
                /* *** */

                /*
                    * Chiudi busta, solo se la pagina in cui sto mettendo i segni
                    * � indicata come chiusura della busta
                    */
                if (ChiudiBusta.Contains(_contapag))
                {
                    _segnoy = _SEGNI[1];
                    if (_InserisciSegniImbustamento == true)
                        _RitornoMessaggio = _ScriviSegno(_segnox, _segnoy, _rotazione, ref _wri, _SpessoreSegni);
                    if (_RitornoMessaggio <= 0)
                        return _RitornoMessaggio;

                    _contasegni++;
                    _InizContaBusta++;
                    _InizContaPagina = 0;
                }
                /* *** */

                /*
                    * Contatore binario 4
                    */
                if (_InizContatoreBinario >= 4)
                {
                    _segnoy = _SEGNI[2];

                    if (_InserisciSegniImbustamento == true)
                        _RitornoMessaggio = _ScriviSegno(_segnox, _segnoy, _rotazione, ref _wri, _SpessoreSegni);
                    if (_RitornoMessaggio <= 0)
                        return _RitornoMessaggio;

                    _contasegni++;
                }
                /* *** */

                /*
                    * Contatore binario 2
                    */
                if (_InizContatoreBinario == 2 || _InizContatoreBinario == 3 || _InizContatoreBinario == 6 || _InizContatoreBinario == 7)
                {
                    _segnoy = _SEGNI[3];

                    if (_InserisciSegniImbustamento == true)
                        _RitornoMessaggio = _ScriviSegno(_segnox, _segnoy, _rotazione, ref _wri, _SpessoreSegni);
                    if (_RitornoMessaggio <= 0)
                        return _RitornoMessaggio;

                    _contasegni++;
                }
                /* *** */

                /*
                    * Contatore binario 1
                    */
                if (_InizContatoreBinario == 1 || _InizContatoreBinario == 3 || _InizContatoreBinario == 5 || _InizContatoreBinario == 7)
                {
                    _segnoy = _SEGNI[4];

                    if (_InserisciSegniImbustamento == true)
                        _RitornoMessaggio = _ScriviSegno(_segnox, _segnoy, _rotazione, ref _wri, _SpessoreSegni);
                    if (_RitornoMessaggio <= 0)
                        return _RitornoMessaggio;

                    _contasegni++;
                }
                /* *** */

                /*
                    * Segno di CLEAR
                    */
                if (_SEGNI.Length >= 10 && _Clear.Contains(_contapag))
                {
                    _segnoy = _SEGNI[5];

                    if (_InserisciSegniImbustamento == true)
                        _RitornoMessaggio = _ScriviSegno(_segnox, _segnoy, _rotazione, ref _wri, _SpessoreSegni);
                    if (_RitornoMessaggio <= 0)
                        return _RitornoMessaggio;

                    _contasegni++;
                }
                /* *** */

                /*
                    * Segni inserti 1 - 2 - 3
                    */
                if (_SEGNI.Length >= 10 && _Inserti.ContainsKey(_contapag))
                {
                    for (byte _ins = 1; _ins <= 3; _ins++)
                    {
                        if (_Inserti[_contapag.ToString()].ToString().IndexOf(_ins.ToString()) > -1)
                        {
                            // 6,7,8 sono le posizioni nell'array dei segni degli inserti
                            _segnoy = _SEGNI[5 + _ins];

                            if (_InserisciSegniImbustamento == true)
                                _RitornoMessaggio = _ScriviSegno(_segnox, _segnoy, _rotazione, ref _wri, _SpessoreSegni);
                            if (_RitornoMessaggio <= 0)
                                return _RitornoMessaggio;

                            _contasegni++;
                        }
                    }
                }
                /* *** */

                /*
                    * Controllo di parit�
                    */
                if ((_contasegni % 2) == 0)
                {
                    if (_SEGNI.Length >= 10)
                        _segnoy = _SEGNI[9];
                    else
                        _segnoy = _SEGNI[5];

                    if (_InserisciSegniImbustamento == true)
                        _RitornoMessaggio = _ScriviSegno(_segnox, _segnoy, _rotazione, ref _wri, _SpessoreSegni);
                    if (_RitornoMessaggio <= 0)
                        return _RitornoMessaggio;
                }
                /* *** */

                _wri.EndText();
                    

                // decremento il contatore binario
                _InizContatoreBinario--;

                _InizContaPagina++;

                return ++_RitornoMessaggio;
            }
            catch (Exception ex)
            {
                LoggerHelper.Log(ex.StackTrace);
                throw ex;
            }
            
        }


        public static int _MettiSegniWriter(ref PdfReader reader, ref PdfContentByte _wri, int _contapag, ref int _InizContaPagina, ArrayList ChiudiBusta, float[] _SEGNI, float _MARGINEDX, string _IMGSIGNRESPECT, float _LARGHEZZAZONASEGNI, float _MARGINEZONASEGNI, float _DISTANZASEGNI, int _SpessoreSegni, ref int _InizContatoreBinario, bool _FR, ref int _InizContaBusta, string _CN, bool _InserisciDtmxImbustamento, bool _InserisciSegniImbustamento, ref ArrayList _Clear, ref Hashtable _Inserti, ref Hashtable _Fogli)
        {
            int _RitornoMessaggio = 0;

            try
            {

                BaseFont _fontsegno = BaseFont.CreateFont(GetFontFolderPath() + "timr45w.ttf", BaseFont.WINANSI, true);


                /* resetto il contatore binario se < 1 */
                // il contatore parte da 7 e va a scendere
                if (_InizContatoreBinario < 1)
                    _InizContatoreBinario = 7;

                DataMatrixImbustamento _dmtx = new DataMatrixImbustamento();
                DataMatrixImbustamentoBB600 _dmtxbb600 = new DataMatrixImbustamentoBB600();

                _dmtx = new DataMatrixImbustamento('0', 0, 0);

                _dmtx.JobId = _CN;
                _dmtx.ProgressivoBusta = _InizContaBusta.ToString().PadLeft(6, '0');
                _dmtx.ProgressivoPagina = _InizContaPagina.ToString().PadLeft(2, '0');
                _dmtx.TotaleFogliBusta = _Fogli[_InizContaBusta].ToString().PadLeft(2, '0');

                if (_Inserti.Count > 0)
                {
                    if (_Inserti.ContainsKey(_contapag))
                    {
                        string _nuoviacc = "";
                        for (byte _ins = 1; _ins <= 3; _ins++)
                        {
                            if (_Inserti[_contapag.ToString()].ToString().IndexOf(_ins.ToString()) > -1)
                            {
                                _nuoviacc += "1";
                            }
                            else
                                _nuoviacc += "0";
                        }
                        _dmtx.Accessori = _nuoviacc;
                    }
                }

                Rectangle _pagina = reader.GetPageSize(_contapag);

                Rectangle _rotazioneret = reader.GetPageSizeWithRotation(_contapag);
                int _rotazione = _rotazioneret.Rotation;
                int _contasegni = 0;

                // altezza
                float _segnoy = _SEGNI[0];

                // larghezza
                float _segnox = _pagina.Width - _MARGINEDX;

                if (_rotazione == 270 || _rotazioneret.Width > _rotazioneret.Height)
                    _segnox = PageSize.A4.Width - _MARGINEDX;

                /*
                    * Mette il bianco nella zona segni
                    */
                if (_InserisciSegniImbustamento == true)
                    _RitornoMessaggio = _MettiImg(_IMGSIGNRESPECT, _segnox - _LARGHEZZAZONASEGNI, _SEGNI[5] - _MARGINEZONASEGNI, _LARGHEZZAZONASEGNI + _MARGINEDX, (_DISTANZASEGNI * 6) + (_MARGINEZONASEGNI * 2), ref _wri);
                if (_RitornoMessaggio <=0)
                    return _RitornoMessaggio;

                if (_InizContaPagina < 1000)
                {
                    ////if (_InserisciDtmxImbustamento && _FileSum["tipo_segni"] != "8") PRIMA ERA COSI' CON QUESTA CONDIZIONE
                    if (_InserisciDtmxImbustamento)
                        _RitornoMessaggio = _InserisciDataMatrix(_dmtx, ref _wri, _dmtx.X, _dmtx.Y, _IMGSIGNRESPECT);
                    ////else if (_InserisciDtmxImbustamento && _FileSum["tipo_segni"] == "8")
                    ////{
                    ////    // il segno va sul retro, i lotti sono sempre F/R per questo tipo di segni
                    ////    //PdfContentByte _wriRetro = stamp2.GetOverContent(_contapag + 1);
                    ////    //_RitornoMessaggio = _InserisciDataMatrixBB600(_dmtxbb600, ref _wriRetro, _dmtxbb600.X, _dmtxbb600.Y, _IMGSIGNRESPECT);
                    ////}

                    if (_RitornoMessaggio <= 0)
                        return _RitornoMessaggio;
                }

                _wri.BeginText();
                _wri.SetFontAndSize(_fontsegno, 30);

                /*
                    * Start
                    */
                if (_InserisciSegniImbustamento == true)
                    _RitornoMessaggio = _ScriviSegno(_segnox, _segnoy, _rotazione, ref _wri, _SpessoreSegni);
                if (_RitornoMessaggio <= 0)
                    return _RitornoMessaggio;

                _contasegni++;
                /* *** */

                /*
                    * Chiudi busta, solo se la pagina in cui sto mettendo i segni
                    * � indicata come chiusura della busta
                    */
                if (ChiudiBusta.Contains(_contapag))
                {
                    _segnoy = _SEGNI[1];
                    if (_InserisciSegniImbustamento == true)
                        _RitornoMessaggio = _ScriviSegno(_segnox, _segnoy, _rotazione, ref _wri, _SpessoreSegni);
                    if (_RitornoMessaggio <= 0)
                        return _RitornoMessaggio;

                    _contasegni++;
                    _InizContaBusta++;
                    _InizContaPagina = 0;
                }
                /* *** */

                /*
                    * Contatore binario 4
                    */
                if (_InizContatoreBinario >= 4)
                {
                    _segnoy = _SEGNI[2];

                    if (_InserisciSegniImbustamento == true)
                        _RitornoMessaggio = _ScriviSegno(_segnox, _segnoy, _rotazione, ref _wri, _SpessoreSegni);
                    if (_RitornoMessaggio <= 0)
                        return _RitornoMessaggio;

                    _contasegni++;
                }
                /* *** */

                /*
                    * Contatore binario 2
                    */
                if (_InizContatoreBinario == 2 || _InizContatoreBinario == 3 || _InizContatoreBinario == 6 || _InizContatoreBinario == 7)
                {
                    _segnoy = _SEGNI[3];

                    if (_InserisciSegniImbustamento == true)
                        _RitornoMessaggio = _ScriviSegno(_segnox, _segnoy, _rotazione, ref _wri, _SpessoreSegni);
                    if (_RitornoMessaggio <= 0)
                        return _RitornoMessaggio;

                    _contasegni++;
                }
                /* *** */

                /*
                    * Contatore binario 1
                    */
                if (_InizContatoreBinario == 1 || _InizContatoreBinario == 3 || _InizContatoreBinario == 5 || _InizContatoreBinario == 7)
                {
                    _segnoy = _SEGNI[4];

                    if (_InserisciSegniImbustamento == true)
                        _RitornoMessaggio = _ScriviSegno(_segnox, _segnoy, _rotazione, ref _wri, _SpessoreSegni);
                    if (_RitornoMessaggio <=0)
                        return _RitornoMessaggio;

                    _contasegni++;
                }
                /* Segno di CLEAR*/
                if (_SEGNI.Length >= 10 && _Clear.Contains(_contapag))
                {
                    _segnoy = _SEGNI[5];

                    if (_InserisciSegniImbustamento == true)
                        _RitornoMessaggio = _ScriviSegno(_segnox, _segnoy, _rotazione, ref _wri, _SpessoreSegni);
                    if (_RitornoMessaggio <=0)
                        return _RitornoMessaggio;

                    _contasegni++;
                }

                /* Segni inserti 1 - 2 - 3*/
                if (_SEGNI.Length >= 10 && _Inserti.ContainsKey(_contapag))
                {
                    for (byte _ins = 1; _ins <= 3; _ins++)
                    {
                        if (_Inserti[_contapag.ToString()].ToString().IndexOf(_ins.ToString()) > -1)
                        {
                            // 6,7,8 sono le posizioni nell'array dei segni degli inserti
                            _segnoy = _SEGNI[5 + _ins];

                            if (_InserisciSegniImbustamento == true)
                                _RitornoMessaggio = _ScriviSegno(_segnox, _segnoy, _rotazione, ref _wri, _SpessoreSegni);
                            if (_RitornoMessaggio <=0)
                                return _RitornoMessaggio;

                            _contasegni++;
                        }
                    }
                }
                /* Controllo di parit�*/
                if ((_contasegni % 2) == 0)
                {
                    if (_SEGNI.Length >= 10)
                        _segnoy = _SEGNI[9];
                    else
                        _segnoy = _SEGNI[5];

                    if (_InserisciSegniImbustamento == true)
                        _RitornoMessaggio = _ScriviSegno(_segnox, _segnoy, _rotazione, ref _wri, _SpessoreSegni);
                    if (_RitornoMessaggio <=0)
                        return _RitornoMessaggio;
                }
                
                _wri.EndText();

                
                // decremento il contatore binario
                _InizContatoreBinario--;

                _InizContaPagina++;

                return ++_RitornoMessaggio;
            }
            catch (Exception ex)
            {
                LoggerHelper.Log(ex.StackTrace);
                throw ex;
                
            }
        }


        /// <summary>
        /// Funzione interna per scrivere la stringa postale 
        /// </summary>
        /// <param name="_StringaPosta">Array di 8 elementi: 
        /// 0 = omologazione
        /// 1 = autorizzazione
        /// 2 = numero lotto
        /// 3 = progressivo documento
        /// 4 = nome bacino di destinazione
        /// 5 = porto
        /// 6 = numero contenitore
        /// </param>
        /// <param name="_wri">Puntatore al documento</param>
        /// <param name="_font">Font da usare per la stringa</param>
        /// <param name="_sizefont">Grandezza font</param>
        /// <param name="_IMGINDX">Punto di partenza orizzontale della stringa</param>
        /// <param name="_IMGINDY">Punto di partenza verticale della stringa</param>
        /// <param name="_INDINTERLINEA">Distanza dall'indirizzo</param>
        /// <param name="_POSTAINTERLINEA">Interlinea per la seconda riga della stringa postale</param>
        private static void _ScriviStringaPostale(string[] _StringaPosta, ref PdfContentByte _wri, ref BaseFont _font, float _sizefont, float _XSTRINGAPOSTA, float _YSTRINGAPOSTA, float _INDINTERLINEA, float _POSTAINTERLINEA)
        {
            _wri.SetFontAndSize(_font, _sizefont);

            if (_StringaPosta[9] != "")
                _YSTRINGAPOSTA += _POSTAINTERLINEA;

            if (_StringaPosta[0] != "" && _StringaPosta[1] != "")
            {
                _wri.SetTextMatrix(_XSTRINGAPOSTA, _YSTRINGAPOSTA);
                _wri.ShowText(_StringaPosta[0] + " " + _StringaPosta[1]);
            }

            _XSTRINGAPOSTA += 150;

            if (_StringaPosta[2] != "" && _StringaPosta[3] != "")
            {
                _wri.SetTextMatrix(_XSTRINGAPOSTA, _YSTRINGAPOSTA + _POSTAINTERLINEA);
                _wri.ShowText(_StringaPosta[2] + "-" + _StringaPosta[3]);
            }

            string _desttariff = "";
            string _formatobusta = "";

            if (_StringaPosta[7] != "")
                _desttariff = _StringaPosta[7] + " ";

            if (_StringaPosta[8] != "")
                _formatobusta = " " + _StringaPosta[8];

            _wri.SetTextMatrix(_XSTRINGAPOSTA, _YSTRINGAPOSTA - _POSTAINTERLINEA);
            _wri.ShowText(_desttariff + _StringaPosta[4] + " P" + _StringaPosta[5] + _formatobusta + "  " + _StringaPosta[6]);

            if (_StringaPosta[9] != "")
            {
                _wri.SetTextMatrix(_XSTRINGAPOSTA, _YSTRINGAPOSTA); // spostato dalla terza riga alla seconda 16/04/2012
                _wri.ShowText(_StringaPosta[9]);
            }
        }

        /// <summary>
        /// Funzione interna per scrivere la stringa postale e il datamatrix
        /// </summary>
        /// <param name="_DataMatrix">Valori per il datamatrix postale</param>
        /// <param name="_StringaPosta">Array di 9 elementi: 
        /// 0 = omologazione
        /// 1 = autorizzazione
        /// 2 = numero lotto
        /// 3 = progressivo documento
        /// 4 = nome bacino di destinazione
        /// 5 = porto
        /// 6 = numero contenitore
        /// 7 = destinazione tariffaria
        /// 8 = formato busta
        /// </param>
        /// <param name="_wri">Puntatore al documento</param>
        /// <param name="_font">Font da usare per la stringa</param>
        /// <param name="_sizefont">Grandezza font</param>
        /// <param name="_IMGINDX">Punto di partenza orizzontale della stringa</param>
        /// <param name="_IMGINDY">Punto di partenza verticale della stringa</param>
        /// <param name="_INDINTERLINEA">Distanza dall'indirizzo</param>
        /// <param name="_POSTAINTERLINEA">Interlinea per la seconda riga della stringa postale</param>
        private static void _ScriviStringaPostale(ref DataMatrixPoste _DataMatrix, string[] _StringaPosta, ref PdfContentByte _wri, ref BaseFont _font, float _sizefont, float _XSTRINGAPOSTA, float _YSTRINGAPOSTA, float _INDINTERLINEA, float _POSTAINTERLINEA, bool _botti)
        {
            _wri.EndText();

            int _xbotti = 0;
            if (_botti)
                _xbotti = 75;

            _InserisciDataMatrix(_DataMatrix, ref _wri, _XSTRINGAPOSTA, _YSTRINGAPOSTA);

            _wri.BeginText();
            _wri.SetFontAndSize(_font, _sizefont);


            if (_StringaPosta[0] != "" && _StringaPosta[1] != "")
            {
                _wri.SetTextMatrix(_XSTRINGAPOSTA - _xbotti, _YSTRINGAPOSTA + 30);
                _wri.ShowText(_StringaPosta[0] + " " + _StringaPosta[1]);
            }

            if (_StringaPosta[9] != "")
                _YSTRINGAPOSTA += _POSTAINTERLINEA;

            _XSTRINGAPOSTA += 150;
            
            if (_StringaPosta[2] != "" && _StringaPosta[3] != "")
            {
                _wri.SetTextMatrix(_XSTRINGAPOSTA - _xbotti, _YSTRINGAPOSTA + _POSTAINTERLINEA);
                _wri.ShowText(_StringaPosta[2] + "-" + _StringaPosta[3]);
            }
            string _desttariff = "";
            string _formatobusta = "";

            if (_StringaPosta[7] != "")
                _desttariff = _StringaPosta[7] + " ";

            if (_StringaPosta[8] != "")
                _formatobusta = " " + _StringaPosta[8];

            _wri.SetTextMatrix(_XSTRINGAPOSTA - _xbotti, _YSTRINGAPOSTA - _POSTAINTERLINEA);
            _wri.ShowText(_desttariff + _StringaPosta[4] + " P" + _StringaPosta[5] + _formatobusta + "  " + _StringaPosta[6]);

            if (_StringaPosta[9] != "")
            {
                _wri.SetTextMatrix(_XSTRINGAPOSTA - _xbotti, _YSTRINGAPOSTA); // spostato dalla terza riga alla seconda 16/04/2012
                _wri.ShowText(_StringaPosta[9]);
            }
        }

        private static void _ScriviStringaPostale(ref C128TNT _Dati, string[] _StringaPosta, ref PdfContentByte _wri, ref BaseFont _font, float _sizefont, float _XSTRINGAPOSTA, float _YSTRINGAPOSTA, float _INDINTERLINEA, float _POSTAINTERLINEA)
        {
            if (_Dati.PosizioneBarCode == C128TNT.Posizione.Indirizzo)
            {
                _InserisciC128(_Dati, ref _wri, _XSTRINGAPOSTA, _YSTRINGAPOSTA);
            }
            else if (_Dati.PosizioneBarCode == C128TNT.Posizione.Raccomandata)
                _InserisciC128(_Dati, ref _wri, 42, 612);

            _wri.SetFontAndSize(_font, _sizefont);

            if (_StringaPosta[9] != "")
                _YSTRINGAPOSTA += _POSTAINTERLINEA;

            _XSTRINGAPOSTA += 180;

            if (_StringaPosta[2] != "" && _StringaPosta[3] != "")
            {
                _wri.SetTextMatrix(_XSTRINGAPOSTA, _YSTRINGAPOSTA + _POSTAINTERLINEA);
                _wri.ShowText(_StringaPosta[2] + "-" + _StringaPosta[3]);
            }

            string _desttariff = "";
            string _formatobusta = "";

            if (_StringaPosta[7] != "")
                _desttariff = _StringaPosta[7] + " ";

            if (_StringaPosta[8] != "")
                _formatobusta = " " + _StringaPosta[8];

            _wri.SetTextMatrix(_XSTRINGAPOSTA, _YSTRINGAPOSTA - _POSTAINTERLINEA);
            _wri.ShowText(_desttariff + _StringaPosta[4] + " P" + _StringaPosta[5] + _formatobusta + "  " + _StringaPosta[6]);

            if (_StringaPosta[9] != "")
            {
                _wri.SetTextMatrix(_XSTRINGAPOSTA, _YSTRINGAPOSTA); // spostato dalla terza riga alla seconda 16/04/2012
                _wri.ShowText(_StringaPosta[9]);          
            }

            if (_StringaPosta[10] != "")
            {
                _wri.SetTextMatrix(50, 660);

                _wri.ShowText(_StringaPosta[10]);
            }
        }

        /// <summary>
        /// Crea una immagine del datamatrix secondo le norme postali e la inserisce nel pdf di riferimento
        /// </summary>
        /// <param name="_DataMatrix">Dati postali</param>
        /// <param name="_wri">Pdf di riferimento</param>
        /// <param name="_X">Coordinata X del datamatrix</param>
        /// <param name="_Y">Coordinata Y del datamatrix</param>
        /// <returns></returns>
        private static int _InserisciDataMatrix(DataMatrixPoste _DataMatrix, ref PdfContentByte _wri, float _X, float _Y)
        {
            int _RitornoMessaggio = 0;

            try
            {
                string _toencode = _DataMatrix.Identificatore
                                    + _DataMatrix.Disponibile1
                                    + _DataMatrix.IdClienteSAP
                                    + _DataMatrix.ContoContrattuale
                                    + _DataMatrix.ClasseProdotto
                                    + _DataMatrix.TipoTariffa
                                    + _DataMatrix.CAP_Destinatario
                                    + _DataMatrix.CodTecnico_Destinatario
                                    + _DataMatrix.CAP_Mittente
                                    + _DataMatrix.CodTecnico_Mittente
                                    + _DataMatrix.Identificativo_ClienteMittente
                                    + _DataMatrix.Disponibile2
                                    + _DataMatrix.Causale
                                    + _DataMatrix.CodOmologazione
                                    + _DataMatrix.Disponibile3;

                _RitornoMessaggio = InserisciDataMatrix(_toencode, ref _wri, _X, _Y, 48, 16, 68, 23);

                return ++_RitornoMessaggio;
            }
            catch (Exception ex)
            {
                LoggerHelper.Log(ex.StackTrace);
                throw ex;
                
            }
        }

        public static int InserisciDataMatrix(string _DataMatrix, ref PdfContentByte _wri, float _X, float _Y, int _wdm, int _hdm, float _wimg, float _himg)
        {
            int _RitornoMessaggio = 0;

            try
            {
                DataMatrix _barcode2d = new DataMatrix(_DataMatrix, _wdm, _hdm);

                //Guid _g = new Guid();
                //string _fileimg = Environment.GetEnvironmentVariable("TEMP") + "\\" + _g.ToString() + ".tmp";                

                //_barcode2d.Image.Save(_fileimg, System.Drawing.Imaging.ImageFormat.Bmp);

                iTextSharp.text.Image _img2d = iTextSharp.text.Image.GetInstance(_barcode2d.Image, System.Drawing.Imaging.ImageFormat.Bmp);
                
                //iTextSharp.text.Image _img2d = iTextSharp.text.Image.GetInstance(_barcode2d.FastBmp.ToByteArray());

                _MettiImg(ref _img2d, _X, _Y, _wimg, _himg, ref _wri);

                //File.Delete(_fileimg);

                return ++_RitornoMessaggio;
            }
            catch (Exception ex)
            {
                LoggerHelper.Log(ex.StackTrace);
                throw ex;
                
            }
        }

        public static int InserisciDataMatrix(string _DataMatrix, ref PdfContentByte _wri, float _X, float _Y, int _wdm, int _hdm, float _wimg, float _himg, System.Drawing.RotateFlipType _rotazione)
        {
            int _RitornoMessaggio = 0;

            try
            {
                DataMatrix _barcode2d = new DataMatrix(_DataMatrix, _wdm, _hdm);

                //Guid _g = new Guid();
                //string _fileimg = Environment.GetEnvironmentVariable("TEMP") + "\\" + _g.ToString() + ".tmp";                

                //_barcode2d.Image.Save(_fileimg, System.Drawing.Imaging.ImageFormat.Bmp);
                _barcode2d.Image.RotateFlip(_rotazione);

                iTextSharp.text.Image _img2d = iTextSharp.text.Image.GetInstance(_barcode2d.Image, System.Drawing.Imaging.ImageFormat.Bmp);
                

                //iTextSharp.text.Image _img2d = iTextSharp.text.Image.GetInstance(_barcode2d.FastBmp.ToByteArray());

                _MettiImg(ref _img2d, _X, _Y, _wimg, _himg, ref _wri);

                //File.Delete(_fileimg);

                return ++_RitornoMessaggio;
            }
            catch (Exception ex)
            {
                LoggerHelper.Log(ex.StackTrace);
                throw ex;
                
            }
        }

        /// <summary>
        /// Inserisce il datamatrix per l'imbustamento
        /// </summary>
        /// <param name="DataMatrixImbustamento">Dati relativi all'imbustamento</param>
        /// <param name="_wri">Pdf di riferimento</param>
        /// <param name="_X">Coordinata X del datamatrix</param>
        /// <param name="_Y">Coordinata Y del datamatrix</param>
        /// <returns></returns>
        private static int _InserisciDataMatrix(DataMatrixImbustamento _DataMatrix, ref PdfContentByte _wri, float _X, float _Y, string _IMGSIGNRESPECT)
        {
            int _RitornoMessaggio = 0;
            string _toencode = "";

            try
            {
                _toencode = _DataMatrix.JobId + _DataMatrix.ProgressivoBusta + _DataMatrix.ProgressivoPagina + _DataMatrix.TotaleFogliBusta + _DataMatrix.Accessori;

                /*
                 * Mette il bianco nella zona segni
                 */
                _RitornoMessaggio = _MettiImg(_IMGSIGNRESPECT, _X - 2, _Y - 10, 28, 40, ref _wri);
                if (_RitornoMessaggio <= 0)
                    return _RitornoMessaggio;

                _RitornoMessaggio = InserisciDataMatrix(_toencode, ref _wri, _X, _Y, 16, 16, 18, 18);

                return ++_RitornoMessaggio;
            }
            catch (Exception ex)
            {
                LoggerHelper.Log(ex.StackTrace);
                throw ex;
            }
        }

        private static int _InserisciDataMatrixBB600(DataMatrixImbustamentoBB600 _DataMatrix, ref PdfContentByte _wri, float _X, float _Y, string _IMGSIGNRESPECT)
        {
            int _RitornoMessaggio = 0;
            string _toencode = "";

            try
            {
                _toencode = _DataMatrix.JobId + _DataMatrix.ProgressivoBusta + _DataMatrix.ProgressivoPagina + _DataMatrix.Accessori + _DataMatrix.TotaleFogliBusta + _DataMatrix.ChiudiBusta + _DataMatrix.ChiamataSlave + _DataMatrix.KickerScatola + _DataMatrix.KickerTappeto;

                /*
                 * Mette il bianco nella zona segni
                 */
                _RitornoMessaggio = _MettiImg(_IMGSIGNRESPECT, _X - 9, _Y - 8, 40, 40, ref _wri);
                if (_RitornoMessaggio <= 0)
                    return _RitornoMessaggio;

                _RitornoMessaggio = InserisciDataMatrix(_toencode, ref _wri, _X, _Y, 22, 22, 23, 23);

                return _RitornoMessaggio;
            }
            catch (Exception ex)
            {
                LoggerHelper.Log(ex.StackTrace);
                throw ex;
                
            }
        }

        private static int _InserisciSegnoTaglio(ref PdfContentByte _cb)
        {
            int _RitornoMessaggio = 0;

            try
            {
                _cb.SaveState();
           
                PdfGState state = new PdfGState();
                state.FillOpacity = 1f;
                _cb.SetGState(state);
                _cb.SetColorFill(Color.BLACK);
                _cb.SetColorStroke(Color.BLACK);
                _cb.SetLineWidth(1);
                _cb.Rectangle(291, 48, 15, 15);
                _cb.Rectangle(291, 90, 15, 15);
                _cb.FillStroke();
                _cb.RestoreState();

                return ++_RitornoMessaggio;
            }
            catch (Exception ex)
            {
                LoggerHelper.Log(ex.StackTrace);
                throw ex;
               
            }
        }

        /// <summary>
        /// Funzione per l'inserimento del barcode C128 sulle buste TNT
        /// </summary>
        /// <param name="_Dati">Struttura di dati</param>
        /// <param name="_wri">Riferimento al pdf</param>
        /// <param name="_X">Coordinata X del posizionamento</param>
        /// <param name="_Y">Coordinata Y del posizionamento</param>
        /// <returns></returns>
        private static int _InserisciC128(C128TNT _Dati, ref PdfContentByte _wri, float _X, float _Y)
        {
            int _RitornoMessaggio = 0;

            try
            {
                string _toencode = _Dati.SiglaAzienda + _Dati.NumeroLotto + _Dati.ProgressivoBusta;

                if (_X < 315 && _Dati.PosizioneBarCode == C128TNT.Posizione.Indirizzo) _X = 315;

                Barcode128 _bcode = new Barcode128();
                _bcode.Font = BaseFont.CreateFont(GetFontFolderPath() + "arial.ttf", BaseFont.WINANSI, BaseFont.EMBEDDED);            
                _bcode.Code = _toencode;
                _bcode.Size = 10;
                _bcode.Baseline = 11;

                //Image _img = Image.GetInstance(_bcode.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White),Color.BLACK );
                Image _img = _bcode.CreateImageWithBarcode(_wri, Color.BLACK, Color.BLACK);
                // calcolo le dimensioni al volo
                _img.SetAbsolutePosition(_X, _Y);

                float _dimheight = _img.PlainHeight;
                float _dimw = _img.PlainWidth;

                _wri.AddImage(_img, _dimw, 0, 0, _dimheight, _X, _Y);
                
                //_MettiImg(ref _img, _X, _Y, 0, 0, ref _wri);

                //_wri.SetFontAndSize(_cf, 10);
                //_wri.SetTextMatrix(_X + 15, _Y);
                //_wri.ShowText(_toencode);

                return ++_RitornoMessaggio;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Errore in funzione _InserisciC128. Dettagli: " + ex.Message + Environment.NewLine + ex.StackTrace);


                LoggerHelper.Log(ex.StackTrace);
                throw ex;
            }
        }

        public static int InserisciC128(string _toencode, ref PdfContentByte _wri, float _X, float _Y, int rotazione)
        {
            int _RitornoMessaggio = 0;

            try
            {
                Barcode128 _bcode = new Barcode128();
                _bcode.Code = _toencode;
                _bcode.Font = BaseFont.CreateFont(GetFontFolderPath() + "arial.ttf", BaseFont.WINANSI, BaseFont.EMBEDDED);            
                _bcode.Size = 8;
                _bcode.Baseline = 8;
                _bcode.BarHeight = 35;
                _bcode.CodeType = Barcode128.CODE_C;
                _bcode.GenerateChecksum = true;
                _bcode.StartStopText = true;
                _bcode.ChecksumText = false;
                _bcode.TextAlignment = Element.ALIGN_CENTER;
                
                Image _img = _bcode.CreateImageWithBarcode(_wri, Color.BLACK, Color.BLACK);
                
                float a = (float)Math.Cos(rotazione * (float)Math.PI / 180) * _img.ScaledHeight;
                float d = (float)Math.Cos(rotazione * (float)Math.PI / 180) * _img.ScaledWidth;

                float b = (float)Math.Sin(rotazione * (float)Math.PI / 180) * _img.ScaledWidth;
                float c = -(float)Math.Sin(rotazione * (float)Math.PI / 180) * _img.ScaledHeight;

                _wri.AddImage(_img, a, b, c, d, _X, _Y);

                return ++_RitornoMessaggio;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Errore in funzione InserisciC128. Dettagli: " + ex.Message + ex.StackTrace);

                LoggerHelper.Log(ex.StackTrace);
                throw ex;
               
            }
        }

        public static int InserisciC128(string _toencode, ref PdfContentByte _wri, float _X, float _Y, int rotazione, float Size, float Baseline, float BarHeight, float Width)
        {
            int _RitornoMessaggio = 0;

            try
            {
                Barcode128 _bcode = new Barcode128();
                _bcode.Code = _toencode;
                _bcode.Font = BaseFont.CreateFont(GetFontFolderPath() + "arial.ttf", BaseFont.WINANSI, BaseFont.EMBEDDED);
                _bcode.Size = Size;
                _bcode.Baseline = Baseline;
                _bcode.BarHeight = BarHeight;
                _bcode.X = Width;
                _bcode.CodeType = Barcode128.CODE_C;
                _bcode.GenerateChecksum = true;
                _bcode.StartStopText = true;
                _bcode.ChecksumText = false;
                
                _bcode.TextAlignment = Element.ALIGN_CENTER;

                Image _img = _bcode.CreateImageWithBarcode(_wri, Color.BLACK, Color.BLACK);

                float a = (float)Math.Cos(rotazione * (float)Math.PI / 180) * _img.ScaledHeight;
                float d = (float)Math.Cos(rotazione * (float)Math.PI / 180) * _img.ScaledWidth;

                float b = (float)Math.Sin(rotazione * (float)Math.PI / 180) * _img.ScaledWidth;
                float c = -(float)Math.Sin(rotazione * (float)Math.PI / 180) * _img.ScaledHeight;

                _wri.AddImage(_img, a, b, c, d, _X, _Y);

                return ++_RitornoMessaggio;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Errore in funzione InserisciC128. Dettagli: " + ex.Message + ex.StackTrace);


                LoggerHelper.Log(ex.StackTrace);
                throw ex;
                
            }
        }

        /// <summary>
        /// Funzione per inserire immagini nel pdf, usata anche per coprire di bianco la zona dei segni e dell'indirizzo
        /// </summary>
        /// <param name="_FileImg">Percorso completo del file gif, jpg, png o pdf (non ancora implementato)</param>
        /// <param name="_POSX">Posizionamento assoluto X</param>
        /// <param name="_POSY">Posizionamento assoluto Y</param>
        /// <param name="_WIDTHIMG">Larghezza</param>
        /// <param name="_HEIGHTIMG">Altezza</param>
        /// <param name="_wri">Contenuto del pdf</param>
        /// <returns></returns>
        public static int _MettiImg(string _FileImg, float _POSX, float _POSY, float _WIDTHIMG, float _HEIGHTIMG, ref PdfContentByte _wri)
        {
            int _RitornoMessaggio = 0;

            try
            {
                Image _Img;

                _Img = Image.GetInstance(_FileImg);

                _Img.SetAbsolutePosition(_POSX, _POSY);

                // calcolo le dimensioni al volo
                if (_WIDTHIMG <= 0 || _HEIGHTIMG <= 0)
                {
                    float _dimheight = _Img.PlainHeight;
                    float _dimw = _Img.PlainWidth;

                    _WIDTHIMG = (_dimw / _Img.DpiX) * 72;
                    _HEIGHTIMG = (_dimheight / _Img.DpiY) * 72;
                }
                
                _wri.AddImage(_Img, _WIDTHIMG, 0, 0, _HEIGHTIMG, _POSX, _POSY);

                return ++_RitornoMessaggio;
            }
            catch (Exception ex)
            {
                LoggerHelper.Log(ex.StackTrace);
                throw ex;

                
            }
        }

        public static int _MettiImg(string _FileImg, float _POSX, float _POSY, float _WIDTHIMG, float _HEIGHTIMG, ref PdfContentByte _wri, float _B, float _C, float _rot)
        {
            int _RitornoMessaggio = 0;
            try
            {
                Image _Img;
              
                _Img = Image.GetInstance(_FileImg);

                _Img.SetAbsolutePosition(_POSX, _POSY);

                // calcolo le dimensioni al volo
                if (_WIDTHIMG <= 0 || _HEIGHTIMG <= 0)
                {
                    float _dimheight = _Img.PlainHeight;
                    float _dimw = _Img.PlainWidth;

                    _WIDTHIMG = (_dimw / _Img.DpiX) * 72;
                    _HEIGHTIMG = (_dimheight / _Img.DpiY) * 72;
                }
                _Img.RotationDegrees = _rot;

                _wri.AddImage(_Img, _WIDTHIMG, _B, _C, _HEIGHTIMG, _POSX, _POSY);

                return ++_RitornoMessaggio;
            }
            catch (Exception ex)
            {
                LoggerHelper.Log(ex.StackTrace);
                throw ex;
                
            }
        }

        /// <summary>
        /// Funzione per inserire immagini nel pdf, usata anche per coprire di bianco la zona dei segni e dell'indirizzo 
        /// </summary>
        /// <param name="_RefImg">Riferimento all'immagine da inserire</param>
        /// <param name="_POSX">Posizionamento assoluto X</param>
        /// <param name="_POSY">Posizionamento assoluto Y</param>
        /// <param name="_WIDTHIMG">Larghezza</param>
        /// <param name="_HEIGHTIMG">Altezza</param>
        /// <param name="_wri">Contenuto del pdf</param>
        /// <returns></returns>
        public static int _MettiImg(ref Image _RefImg, float _POSX, float _POSY, float _WIDTHIMG, float _HEIGHTIMG, ref PdfContentByte _wri)
        {
            int _RitornoMessaggio = 0;
            try
            {
                _RefImg.SetAbsolutePosition(_POSX, _POSY);

                // calcolo le dimensioni al volo
                if (_WIDTHIMG <= 0 || _HEIGHTIMG <= 0)
                {
                    float _dimheight = _RefImg.PlainHeight;
                    float _dimw = _RefImg.PlainWidth;

                    _WIDTHIMG = (_dimw / _RefImg.DpiX) * 72;
                    _HEIGHTIMG = (_dimheight / _RefImg.DpiY) * 72;
                }

                _wri.AddImage(_RefImg, _WIDTHIMG, 0, 0, _HEIGHTIMG, _POSX, _POSY);

                return ++_RitornoMessaggio;
            }
            catch (Exception ex)
            {
                LoggerHelper.Log(ex.StackTrace);
                throw ex;
            }
        }

        /*
         * Funzione interna, solo per scrivere il segno nel pdf tante volte, quanto � lo spessore desiderato
         */
        private static int _ScriviSegno(float _x, float _y, int _rotazione, ref PdfContentByte _wri, int _SpessoreSegni)
        {
            int _RitornoMessaggio = 0;
            try
            {
                /* *****************
                 * Scrivo n volte, per dare pi� spessore al segno
                 * attenzione se la pagina � portrait, altrimenti vanno invertite le coordinate
                 * i paramentri sono 2: larghezza, altezza della posizione del segno
                 * gli altri parametri servono per cambiare l'orientamento del segno
                 * ***************** */
                

                for (int _i = 0; _i < _SpessoreSegni; _i++)
                {
                    if (_rotazione == 0 || _rotazione == 270 || _rotazione == 180)
                        _wri.SetTextMatrix(0, 1, -1, 0, _x, _y - _i);
                    else
                        _wri.SetTextMatrix(_y, _x - _i);

                    _wri.ShowText("|");
                }

                return ++_RitornoMessaggio;
            }
            catch (Exception ex)
            {
                LoggerHelper.Log(ex.StackTrace);
                throw ex;
            }
        }

    
        /// <summary>
        /// Ridimensiona in modo proporzionale ad A4 le pagine del pdf indicato
        /// </summary>
        /// <param name="FileOriginale">Pdf da modificare</param>
        /// <param name="ListaPagine">Lista delle pagine da ridimensionare</param>
        /// <returns></returns>
        public static int scaleToForm(string FileOriginale, ArrayList ListaPagine) 
        {
            int _RitornoMessaggio = 0;

            try 
            {
                Guid _g = Guid.NewGuid();

                string _FilePdfTemp = Environment.GetEnvironmentVariable("TEMP") + "\\" + _g.ToString() + ".pdf.temp";

                PdfReader reader = new PdfReader(FileOriginale);
                int numberOfPages = reader.NumberOfPages;
                float newPageHeight = PageSize.A4.Height;
                float newPageWidth = PageSize.A4.Width;

                using (MemoryStream ms = new MemoryStream())
                {
                    Document document = new Document(new Rectangle(newPageWidth, newPageHeight));

                    //StreamWriter _pdftemp = new StreamWriter(_FilePdfTemp, false);
                    PdfWriter writer = PdfWriter.GetInstance(document, ms);
                    writer.Open();
                    PdfContentByte cb = writer.DirectContent;
                    document.Open();

                    for (int i = 1; i <= numberOfPages; i++)
                    {
                        document.NewPage();
                        PdfImportedPage page = writer.GetImportedPage(reader, i);

                        //int rotation = reader.GetPageRotation(i);
                        float origPageWidth = page.Width;
                        float origPageHeight = page.Height;
                        int rotation = 0;

                        if (origPageWidth > origPageHeight)
                            rotation = 90;
                        else
                            rotation = reader.GetPageRotation(i);

                        float translateHeight = newPageHeight / origPageHeight;
                        float translateWidth = newPageWidth / origPageWidth;
                        //float translateHeight = ((newPageHeight - origPageHeight) / 2) / 10;
                        //float translateWidth = ((newPageWidth - origPageWidth) / 2) / 10;

                        if (ListaPagine.Contains(i))
                        {
                            //System.Drawing.Drawing2D.Matrix _MatrixA4 = new System.Drawing.Drawing2D.Matrix();

                            // esempio:
                            // cb.addTemplate(template, xScale, xRot, yRot, yScale, x, y);
                            if (rotation == 90)
                            {
                                // ruota la pagina
                                cb.AddTemplate(page, 0, 1, -1, 0, newPageWidth, 0);
                            }
                            else if (rotation == 180 || rotation == -180)
                            {
                                //_MatrixA4.Scale(-(newPageWidth / origPageWidth), -(newPageHeight / origPageHeight));
                                //cb.AddTemplate(page, -(newPageWidth / origPageWidth), 0, 0, -(newPageHeight / origPageHeight), -translateWidth, -translateHeight);
                                cb.AddTemplate(page, -translateWidth, 0, 0, -translateHeight, -translateWidth, -translateHeight);
                                //cb.AddTemplate(page, -translateWidth, 0, 0, -translateHeight, 0, 0);
                            }
                            else
                            {
                                //_MatrixA4.Scale(newPageWidth / origPageWidth, newPageHeight / origPageHeight);
                                //cb.AddTemplate(page, newPageWidth / origPageWidth, 0, 0, newPageHeight / origPageHeight, translateWidth, translateHeight);
                                cb.AddTemplate(page, translateWidth, 0, 0, translateHeight, translateWidth, translateHeight);
                                //cb.AddTemplate(page, translateWidth, 0, 0, translateHeight, 0, 0);                            
                            }

                            //cb.Transform(_MatrixA4);
                        }
                        else
                            cb.AddTemplate(page, 0, 0);
                    }

                    document.Close();
                    writer.Close();
                    MemoryStreamToFile(ms, _FilePdfTemp);
                }

                File.Delete(FileOriginale);
                File.Move(_FilePdfTemp, FileOriginale);

                return ++_RitornoMessaggio;
            } 
            catch(Exception ex) 
            {
                LoggerHelper.Log(ex.StackTrace);
                throw ex;
            }
        }

        public static void _EliminaRigheVuote(ref string[] _Indirizzo, int _IndicePrima, int _IndiceDopo)
        {
            _Indirizzo[_IndicePrima] = _Indirizzo[_IndicePrima].Trim();
            if (_Indirizzo[_IndicePrima] == "")
            {
                if (_Indirizzo[_IndicePrima + 1] == "" && _IndicePrima < 9)
                    _EliminaRigheVuote(ref _Indirizzo, _IndicePrima + 1, _IndicePrima);
                else
                {
                    _Indirizzo[_IndiceDopo] = _Indirizzo[_IndicePrima + 1];
                    _Indirizzo[_IndicePrima + 1] = "";
                }
            }

            return;
        }

    }   
}
