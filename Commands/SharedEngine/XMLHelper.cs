﻿using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace MailMerge.Lib
{
    [XmlRootAttribute("pdf", IsNullable = false)]
    public class Pdf 
    {
        public static Pdf Builder()
        {
            Pdf pdf = new Pdf();
            return pdf;
        }
        [XmlAttribute("file-name")]
        public List<string> FileName { get; set; }
        public List<document> document;
    }
    public class document 
    {
        public static document Builder()
        {
            document doc = new document();
            return doc;
        }
        [XmlAttribute("last-page")] public string LastPage;
        [XmlAttribute("start-page")] public string StartPage;
        public List<SheetChange> SheetChange;

    }
    public class SheetChange
    {
        public static SheetChange Builder()
        {
            SheetChange sheetChange = new SheetChange();
            return sheetChange;
        }
        [XmlAttribute("page-number")]
        public string PageNumber { get; set; }
        [XmlAttribute("sheet-type")]
        public string SheetType { get; set; }
    }
    public class XmlBuilder
    {
        readonly XmlSerializer _serializer = new XmlSerializer(typeof(Pdf));
        private XmlBuilder() { }
        public static XmlBuilder Builder()
        {
            XmlBuilder xmlBuilder = new XmlBuilder();
            return xmlBuilder;
        }

        public void SerializeXml(string filename, Pdf xmlData)
        {
            TextWriter writer = new StreamWriter(filename);
            _serializer.Serialize(writer, xmlData);
            writer.Close();
        }

        public Pdf DeserializeXml(string filename)
        {
            Pdf pdf;
            using (TextReader reader = new StreamReader(filename))
            {
                pdf=(Pdf) _serializer.Deserialize(reader);
            }
            return pdf;
        }
        
    }
}
