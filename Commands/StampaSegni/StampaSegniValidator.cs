﻿using System.IO;
using L2C.Contracts.Interfaces;

namespace L2C.Tasks.StampaSegni
{
    class StampaSegniValidator:IValidator
    {
        private readonly string[] _tokens;

        public StampaSegniValidator(string[] tokens)
        {
            _tokens = tokens;
        }
        public bool IsValid()
        {
            return (Path.GetExtension(_tokens[1]) == ".pdf") && (Path.GetExtension(_tokens[3]) == ".pdf") && (_tokens.Length == 4);
        }
    }
}
