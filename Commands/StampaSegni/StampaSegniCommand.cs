﻿using System;
using System.Collections;
using System.IO;
using DLLSharedMethods;
using iTextSharp.text;
using iTextSharp.text.pdf;
using L2C.Contracts.Interfaces.Commands;
using MailMerge.Lib;
using System.Configuration;

namespace L2C.Tasks.StampaSegni
{
    public class StampaSegniCommand : IStampaSegniCommand
    {
        private XmlBuilder _xml;
        public string PdfInputFileName { get; private set; }
        public string XmlInputFileName { get; private set; }
        public string PdfOutputFileName { get; private set; }

        public StampaSegniCommand( string pdfInputFileName, string xmlInputFileName, string pdfOutputFileName)
        {
            PdfOutputFileName = pdfOutputFileName;
            PdfInputFileName = pdfInputFileName;
            XmlInputFileName = xmlInputFileName;
        }

        public void Execute()
        {
            //**********FOR ADDING WHITE PAGE IF YOU DON'T FIND BETTER SOLUTION************************
            //string FILE_PAG_BIANCA = Path.Combine(CommonHelper.PathProcess, "ini", "bianca.pdf");
            //FileInfo filepagBianca = new FileInfo(FILE_PAG_BIANCA);
            
            _xml = XmlBuilder.Builder();
            const float marginedx = 10;
            string imgsignrespect = Path.Combine(ConfigurationManager.AppSettings["currentPath"], "imgsignrespect.gif");
            FileInfo fileImgResp = new FileInfo(imgsignrespect);
            const float larghezzazonasegni = 5;
            const float marginezonasegni = 5;
            const float distanzasegni = 12;
            const int spessoreSegni = 2;
            int inizContatoreBinario = 7;
            const bool fr = true; // <------ se è fronte/retro, per il momento metti false fisso
            ArrayList clear = new ArrayList(); // <------ è sempre vuoto
            Hashtable inserti = new Hashtable(); // <---------- per ora lascia vuoto
            Hashtable fogli = new Hashtable();
            ArrayList chiudiBusta = new ArrayList();
            const float marginesup = 105;
            const float start = 817;

            float[] segni = { 0, 0, 0, 0, 0, 0 }; 
            for (int i = 0; i <= 5; i++)
                segni[i] = start - marginesup - (distanzasegni * i);

            const bool inserisciDtmxImbustamento = false;
            const bool inserisciSegniImbustamento = true;
            using (FileStream ms = new FileStream(PdfOutputFileName,FileMode.Create,FileAccess.Write))
            {
                PdfReader reader = new PdfReader(PdfInputFileName);
                Document doc = new Document();

                PdfWriter writer = PdfWriter.GetInstance(doc, ms);

                writer.PDFXConformance = PdfWriter.PDFA1B;
                writer.CreateXmpMetadata();
                int numTotalePagine = 0;
                fogli.Clear();
                int numpagine = reader.NumberOfPages;
                numTotalePagine += numpagine;
                doc.Open();
                bool flag = false;
                int numberOfPagesForDocument;
                if (int.TryParse(XmlInputFileName, out numberOfPagesForDocument))
                    flag = true;
                int inizContaPagina = 1;
                int inizContaBusta = 1;
                if (flag == false)
                {
                    Pdf pdf = _xml.DeserializeXml(XmlInputFileName);
                    foreach (document t in pdf.document)
                        chiudiBusta.Add(Convert.ToInt32(t.LastPage));
                }

                for (int pagina = 1; pagina <= numpagine; pagina++)
                {
                    fogli.Add(pagina, numTotalePagine);
                    int rotazione = reader.GetPageSizeWithRotation(pagina).Rotation;
                    PdfContentByte writ = writer.DirectContent;
                    PdfImportedPage page = writer.GetImportedPage(reader, pagina);
                    doc.NewPage();
                    if (rotazione == 90 || rotazione == 270)
                    {
                        const float rotate = 90;
                        float x = PageSize.A4.Width;
                        const float y = 0;
                        const float angle = (float)(-rotate * (Math.PI / 180));
                        float xScale = (float)Math.Cos(angle);
                        float yScale = (float)Math.Cos(angle);
                        float xRot = (float)-Math.Sin(angle);
                        float yRot = (float)Math.Sin(angle);
                        writ.AddTemplate(page, xScale, xRot, yRot, yScale, x, y);
                    }
                    else
                    {
                        writ.AddTemplate(page, 0f, 0f);
                    }
                    if (flag)
                        if (pagina % numberOfPagesForDocument == 0)
                            chiudiBusta.Add(pagina);
                    SharedMethods._MettiSegniWriter(ref reader, ref writ, pagina, ref inizContaPagina, chiudiBusta, segni, marginedx, imgsignrespect, larghezzazonasegni, marginezonasegni, distanzasegni, spessoreSegni, ref inizContatoreBinario, fr, ref inizContaBusta, "12345678", inserisciDtmxImbustamento, inserisciSegniImbustamento, ref clear, ref inserti, ref fogli);
                }
                doc.Close();
                reader.Close();
            }
        }
    }
}
