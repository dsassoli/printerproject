﻿using L2C.Contracts.Interfaces;
using L2C.Contracts.Interfaces.Commands;
namespace L2C.Tasks.StampaSegni
{
    class StampaSegniStrategy:IStrategy
    {
        public StampaSegniStrategy(string[] tokens)
        {
            Tokens = tokens;
        }
        public string[] Tokens { get; private set; }
        public IValidator Validator { get{return new StampaSegniValidator(Tokens);} }
        public ICommand Command { get { return new StampaSegniCommand(Tokens[1], Tokens[2], Tokens[3]); } }
    }
}
