﻿using L2C.Contracts.Interfaces;
using L2C.Contracts.Interfaces.Commands;
namespace L2C.Tasks.PdfMerge
{
    public class PdfMergeStrategy:IStrategy
    {
        public PdfMergeStrategy(string[] tokens)
        {
            Tokens = tokens;
        }

        public string[] Tokens { get; private set; }

        public IValidator Validator { get { return new PdfMergeValidator(Tokens); } }

        public ICommand Command { get { return new PdfMergeCommand(Tokens[1], Tokens[2]); } }
    }
}
