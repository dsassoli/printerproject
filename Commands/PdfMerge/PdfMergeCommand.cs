﻿using System;
using System.Collections.Generic;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using DLLSharedMethods;
using MailMerge.Lib;
using L2C.Contracts.Interfaces.Commands;
namespace L2C.Tasks.PdfMerge
{

    public class PdfMergeCommand:ICommand
    {
        public string PdfOutputFile { get; set; }
        public string FolderName { get; set; }
        //variabile inserita per sopperire ad un bug di word durante il mail merge che aggiunge una pagina in fondo.
        public int PageAddedByWord { get; set; }
        public PdfMergeCommand(string folderName, string pdfOutputFile,int pageAddedByWord=0)
        {
            FolderName = folderName;
            PdfOutputFile = pdfOutputFile;
            PageAddedByWord = pageAddedByWord;
        }
        public void Execute()
        {
            string[] files = Directory.GetFiles(FolderName);
            using (MemoryStream ms = new MemoryStream())
            {
                Document doc = new Document();
                PdfSmartCopy pdfCopy = new PdfSmartCopy(doc, ms);
                pdfCopy.PDFXConformance = PdfWriter.PDFA1B;
                pdfCopy.CreateXmpMetadata();
                doc.Open();
                foreach (var percorsoFilePdf in files)
                {
                    PdfReader reader = new PdfReader(percorsoFilePdf);
                    int numpagine = reader.NumberOfPages;
                    for (int I = 1; I <= numpagine-PageAddedByWord; I++)
                    {
                        doc.SetPageSize(reader.GetPageSizeWithRotation(1));
                        PdfImportedPage page = pdfCopy.GetImportedPage(reader, I);
                        pdfCopy.AddPage(page);
                    }
                    //Clean up
                    //pdfCopy.FreeReader(reader);
                    reader.Close();
                }
                //Clean up
                doc.Close();
                SharedMethods.MemoryStreamToFile(ms, PdfOutputFile);
            }
        }
    }
}
