﻿using L2C.Contracts.Interfaces;
namespace L2C.Tasks.PdfMerge
{
    public class PdfMergeValidator:IValidator
    {
        private readonly string[] _tokens;

        public PdfMergeValidator(string[] tokens)
        {
            _tokens = tokens;
        }
        public bool IsValid()
        {
            return _tokens.Length==3;
        }
    }
}
