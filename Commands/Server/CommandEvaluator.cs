using System;
using System.Collections.Generic;
using L2C.Contracts.Interfaces.Commands;
using L2C.Tasks.MailMerge;
using L2C.Tasks.PdfPrinter;
using L2C.Tasks.StampaSegni;
using L2C.Contracts.Interfaces;
using L2C.Tasks.PdfMerge;
namespace L2C.Tasks.Server
{
    class CommandEvaluator : ICommandEvaluator
    {
        private const int CommandNameTokenIndex = 0;
        private const string TokenSeparators = "|";
        public string commandAsString { get; private set; }

        private readonly Dictionary<string, Type> _evaluators = new Dictionary<string, Type>
        {
            {"m", typeof(MailMergeStrategy) },
            {"p", typeof(PdfPrinterStrategy) },
            {"s", typeof(StampaSegniStrategy) },
            {"pm", typeof(PdfMergeStrategy) }
        };

        public CommandEvaluator(string CommandAsString)
        {
            commandAsString = CommandAsString;
        }

        public ICommand Evaluate()
        {
            string[] commandTokens = commandAsString.Split(TokenSeparators.ToCharArray());
            Type strategyType = _evaluators[ commandTokens[CommandNameTokenIndex]];
            IStrategy strategy = (IStrategy)Activator.CreateInstance(strategyType, new object[] { commandTokens });
            if (!strategy.Validator.IsValid())
                throw new InvalidOperationException("Validazione per il comando fallita");
            
            return strategy.Command;
        }

        public void Execute(ICommand command)
        {
           command.Execute();
        }

    }
}