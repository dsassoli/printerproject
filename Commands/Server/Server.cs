﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using L2C.Contracts.Interfaces.Server;
using L2C.Contracts.Interfaces.Commands;
using DLLSharedMethods;
namespace L2C.Tasks.Server
{
    class Server : IServer
    {
        public NetworkStream Stream { get; private set; }
        public TcpClient Client { get; private set; }
        public TcpListener ServerListener { get; private set; }
        public ICommandEvaluator CommandEvaluator { get; private set; }
        public void SetUpConnection(IServerParams serverParams)
        {
            try
            {
                // Start Listening at the specified port
                ServerListener = new TcpListener(serverParams.IpAddress, serverParams.Port);
                ServerListener.Start();
            }
            catch (Exception e)
            {
                LoggerHelper.Log(DateTime.Now + "error: " + e.StackTrace);
            }
        }

        private void WorkMethod(object state)
        {
            Stream = Client.GetStream();
            byte[] msg = {};
            Byte[] bytes = new Byte[256];
            try
            {
                int i = Stream.Read(bytes, 0, bytes.Length);
                // Translate data bytes to a ASCII string.
                string command = Encoding.ASCII.GetString(bytes, 0, i);
                CommandEvaluator commandEvaluator = new CommandEvaluator(command);
                var Command = commandEvaluator.Evaluate();
                commandEvaluator.Execute(Command);
                msg = Encoding.ASCII.GetBytes("success");
            }
            catch (Exception e)
            {
                msg = Encoding.ASCII.GetBytes("failure,"+e);
                LoggerHelper.Log(DateTime.Now + "error: " + e.StackTrace);
            }
            finally
            {
                SendResponseToClient(msg);
                Client.Close();
                Stream.Close();
            }
        }
        public void Listener()
        {
            Client = ServerListener.AcceptTcpClient();
            // Get a stream object for reading and writing
            ThreadPool.QueueUserWorkItem(WorkMethod,Client);
        }

        public void SendResponseToClient(byte[] msg)
        {
            try
            {
                //Send back a response.
                Stream.Write(msg, 0, msg.Length);
            }
            catch (Exception e)
            {
                LoggerHelper.Log(DateTime.Now + "error: errore nel rispondere al cliente," + e.StackTrace);
            }
        }
        
    }
}