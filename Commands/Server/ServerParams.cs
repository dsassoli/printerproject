﻿using System.Net;
using L2C.Contracts.Interfaces.Server;
using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Configuration;

namespace L2C.Tasks.Server
{
    public class ServerParams : IServerParams
    {
        public IPAddress IpAddress { get; private set; }
        public int Port { get; private set; }
        public void GetServerParams()
        {
            using (FileStream fs = File.Open(ConfigurationManager.AppSettings["currentPath"] + "\\serverParams.txt", FileMode.Open))
            {
                byte[] b = new Byte[50];
                UTF8Encoding temp = new UTF8Encoding(true);
                string command = null;
                while (fs.Read(b, 0, b.Length) > 0)
                {
                    command = temp.GetString(b);
                }
                string[] readed = command.Split("\0".ToCharArray());
                readed = readed[0].Split(";".ToCharArray());
                IpAddress = IPAddress.Parse(readed[0].ToString(CultureInfo.InvariantCulture));
                Port = Convert.ToInt32(readed[1]);
            }
        }
    }
}