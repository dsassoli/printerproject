﻿namespace L2C.Contracts.Interfaces.Commands
{
    public interface IPdfPrinterCommand:ICommand
    {
        string PrinterName { get; }
        string FileName { get; }
        string TraySchemeFile { get; }
    }
}
