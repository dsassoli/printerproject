﻿namespace L2C.Contracts.Interfaces.Commands
{
    public interface IMailMergeCommand : ICommand
    {
        string DocTemplate { get; }
        string FileExcelInput { get; }
        string DestinationPdfFileName { get; }
        string DestinationXmlFileName { get; }
    }
}
