﻿namespace L2C.Contracts.Interfaces.Commands
{
    public interface IStampaSegniCommand:ICommand
    {
        string PdfInputFileName { get; }
        string XmlInputFileName { get; }
        string PdfOutputFileName { get; }
    }
}
