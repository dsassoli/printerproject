﻿namespace L2C.Contracts.Interfaces.Commands
{

    public interface ICommandEvaluator
    {
        string commandAsString { get; }
        ICommand Evaluate();
        void Execute(ICommand command);
    }
}
