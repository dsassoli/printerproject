﻿namespace L2C.Contracts.Interfaces.Commands
{
    public interface IPdfMergeCommand:ICommand
    {
        string FolderName { get; }
        string PdfOutputFile { get; }
        string XmlOutputFile { get; }
        int ChangeTrayPage { get; }
    }
}
