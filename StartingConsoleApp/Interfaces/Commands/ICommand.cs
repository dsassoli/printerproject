﻿namespace L2C.Contracts.Interfaces.Commands
{
    public interface ICommand
    {
        void Execute();
    }
}
