﻿using L2C.Contracts.Interfaces.Commands;
namespace L2C.Contracts.Interfaces
{
    public interface IStrategy
    {
        string[] Tokens { get; }

        IValidator Validator { get; }
        ICommand Command { get; }
    }
}
