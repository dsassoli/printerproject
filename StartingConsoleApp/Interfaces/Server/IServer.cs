﻿using System.Net.Sockets;
using L2C.Contracts.Interfaces.Commands;
namespace L2C.Contracts.Interfaces.Server
{
    public interface IServer
    {
        ICommandEvaluator CommandEvaluator { get; }
        void SetUpConnection(IServerParams serverParams);
        void Listener();
        NetworkStream Stream { get; }
        TcpClient Client { get; }
        TcpListener ServerListener { get; }
        void SendResponseToClient(byte[] msg);
    }
}