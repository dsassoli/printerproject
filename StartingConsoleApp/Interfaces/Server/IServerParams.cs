﻿using System.Net;

namespace L2C.Contracts.Interfaces.Server
{
    public interface IServerParams
    {
        IPAddress IpAddress { get; }
        int Port { get; }
        void GetServerParams();
    }
}
