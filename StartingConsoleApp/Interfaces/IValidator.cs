﻿namespace L2C.Contracts.Interfaces
{
    public interface IValidator
    {
        bool IsValid();
    }
}