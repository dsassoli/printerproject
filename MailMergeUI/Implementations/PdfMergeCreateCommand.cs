﻿using MailMergeUI.Interfaces;

namespace MailMergeUI.Implementations
{
    class PdfMergeCreateCommand:ICreateCommand
    {
        public string command { get; private set; }
        private string InputPath { get;  set; }
        private string OutputFile { get; set; }
        public PdfMergeCreateCommand(string inputPath,string outputFile)
        {
            InputPath = inputPath;
            OutputFile = outputFile;
        }
        public string CreateCommand()
        {
            command = "pm|" + InputPath + "|" + OutputFile;
            return command;
        }
    }
}
