﻿using System;
using MailMergeUI.Interfaces;

namespace MailMergeUI.Implementations
{
    class SignsPanelCreateCommand:ICreateCommand
    {
        public string command { get; private set; }
        private readonly string _destinationPdfFileName;
        private readonly string _destinationXmlFileName;
        private readonly int _nPagForDoc;
        private readonly string _excelFileBox;
        private readonly string _signsPdfFileName;
        private readonly bool _mergePanelVisible;
        private readonly string _templateFileBox;

        public SignsPanelCreateCommand(string destinationPdfFileName, string destinationXmlFileName, decimal nPagForDoc,
            string signsPdfFileName, string excelFileBox, bool mergePanelVisible, string templateFileBox)
        {
            _destinationPdfFileName = destinationPdfFileName;
            _destinationXmlFileName = destinationXmlFileName;
            _nPagForDoc = Convert.ToInt32(nPagForDoc);
            _signsPdfFileName = signsPdfFileName;
            _excelFileBox = excelFileBox;
            _templateFileBox = templateFileBox;
            _mergePanelVisible = mergePanelVisible;
        }

        public string CreateCommand()
        {
            if (_mergePanelVisible)
            {
                command = "s|" + _destinationPdfFileName + "|";
                if (_nPagForDoc == 0)
                    command += _destinationXmlFileName + "|";
                else
                    command += _nPagForDoc + "|";
            }
            else
            {
                command = "s|" + _templateFileBox + "|";
                if (_nPagForDoc == 0)
                    command += _excelFileBox + "|";
                else
                    command += _nPagForDoc + "|";
            }
            command += _signsPdfFileName;
            return command;
        }
    }
}
