﻿using System;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using MailMergeUI.Interfaces;

namespace MailMergeUI.Implementations
{
    class Client : IClient
    {
        private TcpClient _tcpClient;
        private NetworkStream _stream;

        public void EstablishConnection(IServerParams serverParams)
        {
            try
            {
                _tcpClient = new TcpClient(serverParams.IpAddress, serverParams.Port);
                _stream = _tcpClient.GetStream();
            }
            catch (SocketException)
            {
               
                MessageBox.Show("Server Not Found");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        public void CloseConnection()
        {
            _stream.Close();
            _tcpClient.Close();
        }

        public void SendCommand(string command)
        {
            try
            {
                Byte[] data = Encoding.ASCII.GetBytes(command);
                _stream.Write(data, 0, data.Length);
            }
            catch (SocketException se)
            {
                MessageBox.Show("Server Not Found " + se);
            }
        }

        public string CommandSucceeded()
        {
            Byte[] data = new Byte[256];
            string response;
            try
            {
                Int32 bytes = _stream.Read(data, 0, data.Length);
                String responseData = Encoding.ASCII.GetString(data, 0, bytes);
                string state = responseData.Split(",".ToCharArray()).First();
                response = state == "success" ? "operazione eseguita con successo" : responseData;
            }
            catch (SocketException)
            {
                response = "Server not found";
            }
            return response;

        }
    }
}