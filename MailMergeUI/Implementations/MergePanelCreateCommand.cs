﻿using MailMergeUI.Interfaces;

namespace MailMergeUI.Implementations
{
    class MergePanelCreateCommand:ICreateCommand
    {
        public string command { get; private set; }
        private readonly string _templateFileBox;
        private readonly string _excelFileBox;
        private readonly string _destinationPdfFileFilename;
        private readonly string _destinationXmlFileName;

        public MergePanelCreateCommand(string destinationXmlFileName, string destinationPdfFileFilename, string excelFileBox, string templateFileBox)
        {
            _destinationXmlFileName = destinationXmlFileName;
            _destinationPdfFileFilename = destinationPdfFileFilename;
            _excelFileBox = excelFileBox;
            _templateFileBox = templateFileBox;
        }

        public string CreateCommand()
        {
            command = "m|" + _templateFileBox + "|" + _excelFileBox + "|" + _destinationPdfFileFilename + "|" + _destinationXmlFileName+"|";
            return command;
        }
    }
}
