﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Windows.Forms;
using MailMergeUI.Interfaces;
using System.Configuration;

namespace MailMergeUI.Implementations
{
    public class ServerParams : IServerParams
    {
        private static ServerParams _instance;

        public static ServerParams Instance { get { return _instance ?? (_instance = new ServerParams()); } }

        public string IpAddress { get; private set; }
        public int Port { get; private set; }
        public void GetServerParams()
        {

            using (FileStream fs = File.Open(ConfigurationManager.AppSettings["currentPath"] + "\\serverParams.txt", FileMode.Open))
            {
                byte[] b = new Byte[50];
                UTF8Encoding temp = new UTF8Encoding(true);
                string command = null;
                while (fs.Read(b, 0, b.Length) > 0)
                {
                    command=temp.GetString(b);
                }
                string[] readed = command.Split("\0".ToCharArray());
                readed = readed[0].Split(";".ToCharArray());
                IpAddress = readed[0].ToString(CultureInfo.InvariantCulture);
                Port = Convert.ToInt32(readed[1]);
            }
        }
    }
}