﻿using System.Collections.Generic;
using MailMergeUI.Interfaces;

namespace MailMergeUI.Implementations
{
    class PrintPanelCreateCommand:ICreateCommand
    {
        public string command { get; private set; }
        private readonly string _printerNameBox;
        private readonly string _fileToPrint;
        private readonly string _toPrintXml;

        public PrintPanelCreateCommand(string toPrintXml, string fileToPrint, string printerNameBox)
        {
            _toPrintXml = toPrintXml;
            _fileToPrint = fileToPrint;
            _printerNameBox = printerNameBox;
        }

        public string CreateCommand()
        {
            command = "p|" + _printerNameBox+ "|" + _fileToPrint + "|" + _toPrintXml+ "|";
            foreach (KeyValuePair<string, string> keyValuePair in Form1.CassettoFromTipoCarta)
            {
                command = command + keyValuePair.Key + ":" + keyValuePair.Value + "/";
            }
            return command;
        }
    }
}
