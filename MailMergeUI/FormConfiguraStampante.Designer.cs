﻿namespace MailMergeUI
{
    partial class FormConfiguraStampante
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.NewPrinter = new System.Windows.Forms.ComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Cassetto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoCarta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaveButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Configura Nuova Stampante";
            // 
            // NewPrinter
            // 
            this.NewPrinter.FormattingEnabled = true;
            this.NewPrinter.Location = new System.Drawing.Point(15, 35);
            this.NewPrinter.Name = "NewPrinter";
            this.NewPrinter.Size = new System.Drawing.Size(353, 21);
            this.NewPrinter.TabIndex = 1;
            this.NewPrinter.SelectedIndexChanged += new System.EventHandler(this.NewPrinter_SelectedIndexChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Cassetto,
            this.TipoCarta});
            this.dataGridView1.Location = new System.Drawing.Point(15, 62);
            this.dataGridView1.MinimumSize = new System.Drawing.Size(348, 150);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(348, 150);
            this.dataGridView1.TabIndex = 2;
            // 
            // Cassetto
            // 
            this.Cassetto.HeaderText = "Cassetto";
            this.Cassetto.Name = "Cassetto";
            // 
            // TipoCarta
            // 
            this.TipoCarta.HeaderText = "Tipo Carta";
            this.TipoCarta.Name = "TipoCarta";
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(15, 231);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(75, 23);
            this.SaveButton.TabIndex = 3;
            this.SaveButton.Text = "Salva";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // FormConfiguraStampante
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 280);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.NewPrinter);
            this.Controls.Add(this.label1);
            this.Name = "FormConfiguraStampante";
            this.Text = "Form2";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox NewPrinter;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cassetto;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoCarta;
        private System.Windows.Forms.Button SaveButton;
    }
}