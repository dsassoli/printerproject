﻿namespace MailMergeUI.Interfaces
{
    public interface ICreateCommand
    {
        string command { get; }
        string CreateCommand();
    }
}
