﻿namespace MailMergeUI.Interfaces
{
    public interface IClient
    {
        void EstablishConnection(IServerParams serverParams);
        void CloseConnection();
        void SendCommand(string command);
        string CommandSucceeded();
    }
}