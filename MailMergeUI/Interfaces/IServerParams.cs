﻿namespace MailMergeUI.Interfaces
{
    public interface IServerParams
    {
        string IpAddress { get; }
        int Port { get;  }
        void GetServerParams();
    }
}