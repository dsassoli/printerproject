﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Foxit.PDF.Printing;

namespace MailMergeUI
{
    public partial class FormConfiguraStampante : Form
    {
        //private Dictionary<string, string> CassettoFromTipoCarta { get; set; }

        //private FoxItPDFPrinter printer;
        public FormConfiguraStampante()
        {
            InitializeComponent();
            dataGridView1.Hide();
            SaveButton.Hide();
            foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
                NewPrinter.Items.Add(printer);
        }

        private void NewPrinter_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Refresh();
            string printerName = NewPrinter.Text;
            PrintJob printJob = new PrintJob(printerName);
            dataGridView1.Columns["Cassetto"].ReadOnly = true;
            dataGridView1.Columns["Cassetto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            foreach (PaperSource paperSource in printJob.Printer.PaperSources.Where(paperSource => paperSource.Name != "Selezione automatica").ToList())
                dataGridView1.Rows.Add(paperSource.Name, "");
            dataGridView1.Show();
            SaveButton.Show();
            Size = new Size(392, 298);
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            Form1.CassettoFromTipoCarta=new Dictionary<string, string>();
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                //int j = i + 1;
                Form1.CassettoFromTipoCarta[(string)dataGridView1.Rows[i].Cells["TipoCarta"].Value] = (i+1).ToString(CultureInfo.InvariantCulture);
            }
            Close();
        }

    }
}
