﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using MailMergeUI.Implementations;
using MailMergeUI.Interfaces;

namespace MailMergeUI
{
    public partial class Form1 : Form
    {
        public static Dictionary<string, string> CassettoFromTipoCarta { get; set; }
        private readonly IClient _client;
        public Form1()
        {
            _client = new Client();
            InitializeComponent();
            MergePanel.Hide();
            SignsPanel.Hide();
            PrintPanel.Hide();
            PdfMergePanel.Hide();
            foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
                printerNameBox.Items.Add(printer);
        }
        #region standards
        private void MergeWithSignsButton(object sender, EventArgs e)
        {
            ServerParams.Instance.GetServerParams();
            string command = "";
            if (MergePanel.Visible)
            {
                List<TextBox> toBeCompiled = new List<TextBox> { DestinationXMLFileName, destinationPDFFileName };
                if (AreFieldsCompiled(toBeCompiled))
                {
                    _client.EstablishConnection(ServerParams.Instance);
                    MergePanelCreateCommand mergePanelCreateCommand = new MergePanelCreateCommand(DestinationXMLFileName.Text, destinationPDFFileName.Text, ExcelFileBox.Text,TemplateFileBox.Text);
                    command = mergePanelCreateCommand.CreateCommand();
                    _client.SendCommand(command);
                    MessageBox.Show(_client.CommandSucceeded());
                    _client.CloseConnection();
                }
            }
            if (PdfMergePanel.Visible)
            {
                List<TextBox> toBeCompiled = new List<TextBox> { PdfMergeInput, PdfMergeOutput };
                if (AreFieldsCompiled(toBeCompiled))
                {
                    _client.EstablishConnection(ServerParams.Instance);
                    PdfMergeCreateCommand pdfMergeCreatecommand = new PdfMergeCreateCommand(PdfMergeInput.Text, PdfMergeOutput.Text);
                    command = pdfMergeCreatecommand.CreateCommand();
                    _client.SendCommand(command);
                    MessageBox.Show(_client.CommandSucceeded());
                    _client.CloseConnection();
                }
            }
            if (SignsPanel.Visible)
            {
                List<TextBox> toBeCompiled = new List<TextBox> { signsPDFFileName };
                if (AreFieldsCompiled(toBeCompiled))
                {
                    _client.EstablishConnection(ServerParams.Instance);
                    SignsPanelCreateCommand signsPanelCreateCommand = new SignsPanelCreateCommand(destinationPDFFileName.Text, DestinationXMLFileName.Text, NPagForDoc.Value, signsPDFFileName.Text, ExcelFileBox.Text, MergePanel.Visible, TemplateFileBox.Text);
                    command = signsPanelCreateCommand.CreateCommand();
                    _client.SendCommand(command);
                    MessageBox.Show(_client.CommandSucceeded());
                    _client.CloseConnection();
                }
            }
            if (PrintPanel.Visible)
            {
                List<TextBox> toBeCompiled = new List<TextBox> { FileToPrint, ToPRintXml };
                if (AreFieldsCompiled(toBeCompiled))
                {
                    _client.EstablishConnection(ServerParams.Instance);
                    PrintPanelCreateCommand printPanelCreateCommand=new PrintPanelCreateCommand(ToPRintXml.Text,FileToPrint.Text,printerNameBox.Text);
                    command=printPanelCreateCommand.CreateCommand();
                    _client.SendCommand(command);
                    MessageBox.Show(_client.CommandSucceeded());
                    _client.CloseConnection();
                }
            }
        }
      
        #endregion
        #region grafica
        private void TemplateFileButton(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Word or PDF Files|*.doc;*.docx;*.pdf";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                TemplateFileBox.Text = openFileDialog1.FileName;
        }
        private void ExcelFileButton(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Excel or Xml Files|*.xls;*.xlsx;*.xlsm;*.xml";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                ExcelFileBox.Text = openFileDialog1.FileName;
        }
        private bool AreFieldsCompiled(IEnumerable<TextBox> toBeCompiled)
        {
            foreach (TextBox field in toBeCompiled.Where(field => String.IsNullOrEmpty(field.Text)))
            {
                fieldRequired.SetError(field, "Inserisci un valore per i campi obbligatori");
                return false;
            }
            return true;
        }

        private void buttonXmlMerge_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "XML Files|*.xml";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                DestinationXMLFileName.Text = saveFileDialog1.FileName;
                fieldRequired.Clear();
            }
        }

        private void buttonPDFMerge_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "PDF Files|*.pdf";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                destinationPDFFileName.Text = saveFileDialog1.FileName;
                fieldRequired.Clear();
            }
        }

        private void buttonPDFSigns_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "PDF Files|*.pdf";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                signsPDFFileName.Text = saveFileDialog1.FileName;
                fieldRequired.Clear();
            }
        }

        private void MailMergeMenu_Click(object sender, EventArgs e)
        {
            if (MergePanel.Visible)
                MergePanel.Hide();
            else
                MergePanel.Show();
        }

        private void PrintSignsMenu_Click(object sender, EventArgs e)
        {
            if (SignsPanel.Visible)
                SignsPanel.Hide();
            else
                SignsPanel.Show();
        }

        private void TemplateFileBox_TextChanged(object sender, EventArgs e)
        {
            fieldRequired.Clear();
        }

        private void ExcelFileBox_TextChanged(object sender, EventArgs e)
        {
            fieldRequired.Clear();
        }

        private void PrintMenu_Click(object sender, EventArgs e)
        {
            if (PrintPanel.Visible)
                PrintPanel.Hide();
            else
                PrintPanel.Show();
        }

        private void configuraNuovaStampanteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormConfiguraStampante settingsForm=new FormConfiguraStampante();
            settingsForm.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "PDF Files|*.pdf";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                FileToPrint.Text = saveFileDialog1.FileName;
                fieldRequired.Clear();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "XML files|*.xml";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                ToPRintXml.Text = saveFileDialog1.FileName;
                fieldRequired.Clear();
            }
        }

        private void menuButtonPdfMerge_Click(object sender, EventArgs e)
        {
            if (PdfMergePanel.Visible)
                PdfMergePanel.Hide();
            else
                PdfMergePanel.Show();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "PDF Files|*.pdf";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                PdfMergeOutput.Text = saveFileDialog1.FileName;
                fieldRequired.Clear();
            }
        }

        private void Browse_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                PdfMergeInput.Text = folderBrowserDialog1.SelectedPath;
                fieldRequired.Clear();
            }
        }
        #endregion
    }
}
