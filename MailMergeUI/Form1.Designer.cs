﻿using System.Windows.Forms;

namespace MailMergeUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TemplateFile = new System.Windows.Forms.Label();
            this.ExcelFile = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.TemplateFileBox = new System.Windows.Forms.TextBox();
            this.ExcelFileBox = new System.Windows.Forms.TextBox();
            this.buttonTempalteFile = new System.Windows.Forms.Button();
            this.buttonExcelFile = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.DestinationXMLFileName = new System.Windows.Forms.TextBox();
            this.buttonMergeANDSegni = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.destinationPDFFileName = new System.Windows.Forms.TextBox();
            this.buttonPDFMerge = new System.Windows.Forms.Button();
            this.buttonXLMerge = new System.Windows.Forms.Button();
            this.buttonPDFSigns = new System.Windows.Forms.Button();
            this.signsPDFFileName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.fieldRequired = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.menuButtonPdfMerge = new System.Windows.Forms.Button();
            this.PrintMenu = new System.Windows.Forms.Button();
            this.PrintSignsMenu = new System.Windows.Forms.Button();
            this.MailMergeMenu = new System.Windows.Forms.Button();
            this.TemplatePanel = new System.Windows.Forms.Panel();
            this.MergePanel = new System.Windows.Forms.Panel();
            this.SignsPanel = new System.Windows.Forms.Panel();
            this.NPagForDoc = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.PrintPanel = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.ToPRintXml = new System.Windows.Forms.TextBox();
            this.FileToPrint = new System.Windows.Forms.TextBox();
            this.printerNameBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.modificaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuraNuovaStampanteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PdfMergePanel = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.Browse = new System.Windows.Forms.Button();
            this.PdfMergeOutput = new System.Windows.Forms.TextBox();
            this.PdfMergeInput = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.fieldRequired)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.TemplatePanel.SuspendLayout();
            this.MergePanel.SuspendLayout();
            this.SignsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NPagForDoc)).BeginInit();
            this.PrintPanel.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.PdfMergePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // TemplateFile
            // 
            this.TemplateFile.AutoSize = true;
            this.TemplateFile.Location = new System.Drawing.Point(3, 9);
            this.TemplateFile.Name = "TemplateFile";
            this.TemplateFile.Size = new System.Drawing.Size(67, 13);
            this.TemplateFile.TabIndex = 1;
            this.TemplateFile.Text = "Template file";
            // 
            // ExcelFile
            // 
            this.ExcelFile.AutoSize = true;
            this.ExcelFile.Location = new System.Drawing.Point(3, 35);
            this.ExcelFile.Name = "ExcelFile";
            this.ExcelFile.Size = new System.Drawing.Size(49, 13);
            this.ExcelFile.TabIndex = 2;
            this.ExcelFile.Text = "Excel file";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // TemplateFileBox
            // 
            this.TemplateFileBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TemplateFileBox.Location = new System.Drawing.Point(94, 6);
            this.TemplateFileBox.Name = "TemplateFileBox";
            this.TemplateFileBox.Size = new System.Drawing.Size(530, 20);
            this.TemplateFileBox.TabIndex = 3;
            this.TemplateFileBox.TextChanged += new System.EventHandler(this.TemplateFileBox_TextChanged);
            // 
            // ExcelFileBox
            // 
            this.ExcelFileBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ExcelFileBox.Location = new System.Drawing.Point(94, 32);
            this.ExcelFileBox.Name = "ExcelFileBox";
            this.ExcelFileBox.Size = new System.Drawing.Size(530, 20);
            this.ExcelFileBox.TabIndex = 4;
            this.ExcelFileBox.TextChanged += new System.EventHandler(this.ExcelFileBox_TextChanged);
            // 
            // buttonTempalteFile
            // 
            this.buttonTempalteFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonTempalteFile.Location = new System.Drawing.Point(644, 4);
            this.buttonTempalteFile.Name = "buttonTempalteFile";
            this.buttonTempalteFile.Size = new System.Drawing.Size(75, 23);
            this.buttonTempalteFile.TabIndex = 5;
            this.buttonTempalteFile.Text = "Browse";
            this.buttonTempalteFile.UseVisualStyleBackColor = true;
            this.buttonTempalteFile.Click += new System.EventHandler(this.TemplateFileButton);
            // 
            // buttonExcelFile
            // 
            this.buttonExcelFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExcelFile.Location = new System.Drawing.Point(644, 30);
            this.buttonExcelFile.Name = "buttonExcelFile";
            this.buttonExcelFile.Size = new System.Drawing.Size(75, 23);
            this.buttonExcelFile.TabIndex = 6;
            this.buttonExcelFile.Text = "Browse";
            this.buttonExcelFile.UseVisualStyleBackColor = true;
            this.buttonExcelFile.Click += new System.EventHandler(this.ExcelFileButton);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Nome xml merge";
            // 
            // DestinationXMLFileName
            // 
            this.DestinationXMLFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DestinationXMLFileName.Location = new System.Drawing.Point(95, 12);
            this.DestinationXMLFileName.Name = "DestinationXMLFileName";
            this.DestinationXMLFileName.Size = new System.Drawing.Size(530, 20);
            this.DestinationXMLFileName.TabIndex = 12;
            // 
            // buttonMergeANDSegni
            // 
            this.buttonMergeANDSegni.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMergeANDSegni.Location = new System.Drawing.Point(700, 445);
            this.buttonMergeANDSegni.Name = "buttonMergeANDSegni";
            this.buttonMergeANDSegni.Size = new System.Drawing.Size(284, 23);
            this.buttonMergeANDSegni.TabIndex = 16;
            this.buttonMergeANDSegni.Text = "Esegui Operazione";
            this.buttonMergeANDSegni.UseVisualStyleBackColor = true;
            this.buttonMergeANDSegni.Click += new System.EventHandler(this.MergeWithSignsButton);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Nome pdf merge";
            // 
            // destinationPDFFileName
            // 
            this.destinationPDFFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.destinationPDFFileName.Location = new System.Drawing.Point(95, 42);
            this.destinationPDFFileName.Name = "destinationPDFFileName";
            this.destinationPDFFileName.Size = new System.Drawing.Size(530, 20);
            this.destinationPDFFileName.TabIndex = 19;
            // 
            // buttonPDFMerge
            // 
            this.buttonPDFMerge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPDFMerge.Location = new System.Drawing.Point(644, 39);
            this.buttonPDFMerge.Name = "buttonPDFMerge";
            this.buttonPDFMerge.Size = new System.Drawing.Size(75, 23);
            this.buttonPDFMerge.TabIndex = 20;
            this.buttonPDFMerge.Text = "Browse";
            this.buttonPDFMerge.UseVisualStyleBackColor = true;
            this.buttonPDFMerge.Click += new System.EventHandler(this.buttonPDFMerge_Click);
            // 
            // buttonXLMerge
            // 
            this.buttonXLMerge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonXLMerge.Location = new System.Drawing.Point(644, 10);
            this.buttonXLMerge.Name = "buttonXLMerge";
            this.buttonXLMerge.Size = new System.Drawing.Size(75, 23);
            this.buttonXLMerge.TabIndex = 21;
            this.buttonXLMerge.Text = "Browse";
            this.buttonXLMerge.UseVisualStyleBackColor = true;
            this.buttonXLMerge.Click += new System.EventHandler(this.buttonXmlMerge_Click);
            // 
            // buttonPDFSigns
            // 
            this.buttonPDFSigns.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPDFSigns.Location = new System.Drawing.Point(644, 8);
            this.buttonPDFSigns.Name = "buttonPDFSigns";
            this.buttonPDFSigns.Size = new System.Drawing.Size(75, 23);
            this.buttonPDFSigns.TabIndex = 26;
            this.buttonPDFSigns.Text = "Browse";
            this.buttonPDFSigns.UseVisualStyleBackColor = true;
            this.buttonPDFSigns.Click += new System.EventHandler(this.buttonPDFSigns_Click);
            // 
            // signsPDFFileName
            // 
            this.signsPDFFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.signsPDFFileName.Location = new System.Drawing.Point(119, 10);
            this.signsPDFFileName.Name = "signsPDFFileName";
            this.signsPDFFileName.Size = new System.Drawing.Size(505, 20);
            this.signsPDFFileName.TabIndex = 25;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Nome pdf segni";
            // 
            // fieldRequired
            // 
            this.fieldRequired.ContainerControl = this;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.menuButtonPdfMerge);
            this.groupBox1.Controls.Add(this.PrintMenu);
            this.groupBox1.Controls.Add(this.PrintSignsMenu);
            this.groupBox1.Controls.Add(this.MailMergeMenu);
            this.groupBox1.Location = new System.Drawing.Point(12, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(199, 315);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Operazioni";
            // 
            // menuButtonPdfMerge
            // 
            this.menuButtonPdfMerge.Location = new System.Drawing.Point(7, 205);
            this.menuButtonPdfMerge.Name = "menuButtonPdfMerge";
            this.menuButtonPdfMerge.Size = new System.Drawing.Size(179, 23);
            this.menuButtonPdfMerge.TabIndex = 4;
            this.menuButtonPdfMerge.Text = "Pdf Merge";
            this.menuButtonPdfMerge.UseVisualStyleBackColor = true;
            this.menuButtonPdfMerge.Click += new System.EventHandler(this.menuButtonPdfMerge_Click);
            // 
            // PrintMenu
            // 
            this.PrintMenu.Location = new System.Drawing.Point(7, 149);
            this.PrintMenu.Name = "PrintMenu";
            this.PrintMenu.Size = new System.Drawing.Size(179, 23);
            this.PrintMenu.TabIndex = 3;
            this.PrintMenu.Text = "Stampa";
            this.PrintMenu.UseVisualStyleBackColor = true;
            this.PrintMenu.Click += new System.EventHandler(this.PrintMenu_Click);
            // 
            // PrintSignsMenu
            // 
            this.PrintSignsMenu.Location = new System.Drawing.Point(7, 88);
            this.PrintSignsMenu.Name = "PrintSignsMenu";
            this.PrintSignsMenu.Size = new System.Drawing.Size(179, 23);
            this.PrintSignsMenu.TabIndex = 2;
            this.PrintSignsMenu.Text = "Inserisci Segni";
            this.PrintSignsMenu.UseVisualStyleBackColor = true;
            this.PrintSignsMenu.Click += new System.EventHandler(this.PrintSignsMenu_Click);
            // 
            // MailMergeMenu
            // 
            this.MailMergeMenu.Location = new System.Drawing.Point(7, 31);
            this.MailMergeMenu.Name = "MailMergeMenu";
            this.MailMergeMenu.Size = new System.Drawing.Size(179, 23);
            this.MailMergeMenu.TabIndex = 1;
            this.MailMergeMenu.Text = "Mail Merge";
            this.MailMergeMenu.UseVisualStyleBackColor = true;
            this.MailMergeMenu.Click += new System.EventHandler(this.MailMergeMenu_Click);
            // 
            // TemplatePanel
            // 
            this.TemplatePanel.Controls.Add(this.TemplateFile);
            this.TemplatePanel.Controls.Add(this.TemplateFileBox);
            this.TemplatePanel.Controls.Add(this.buttonTempalteFile);
            this.TemplatePanel.Controls.Add(this.ExcelFile);
            this.TemplatePanel.Controls.Add(this.ExcelFileBox);
            this.TemplatePanel.Controls.Add(this.buttonExcelFile);
            this.TemplatePanel.Location = new System.Drawing.Point(255, 24);
            this.TemplatePanel.Name = "TemplatePanel";
            this.TemplatePanel.Size = new System.Drawing.Size(722, 59);
            this.TemplatePanel.TabIndex = 30;
            // 
            // MergePanel
            // 
            this.MergePanel.Controls.Add(this.label1);
            this.MergePanel.Controls.Add(this.DestinationXMLFileName);
            this.MergePanel.Controls.Add(this.buttonXLMerge);
            this.MergePanel.Controls.Add(this.label2);
            this.MergePanel.Controls.Add(this.destinationPDFFileName);
            this.MergePanel.Controls.Add(this.buttonPDFMerge);
            this.MergePanel.Location = new System.Drawing.Point(255, 82);
            this.MergePanel.Name = "MergePanel";
            this.MergePanel.Size = new System.Drawing.Size(722, 69);
            this.MergePanel.TabIndex = 31;
            // 
            // SignsPanel
            // 
            this.SignsPanel.Controls.Add(this.NPagForDoc);
            this.SignsPanel.Controls.Add(this.label4);
            this.SignsPanel.Controls.Add(this.label3);
            this.SignsPanel.Controls.Add(this.buttonPDFSigns);
            this.SignsPanel.Controls.Add(this.signsPDFFileName);
            this.SignsPanel.Location = new System.Drawing.Point(255, 150);
            this.SignsPanel.Name = "SignsPanel";
            this.SignsPanel.Size = new System.Drawing.Size(722, 66);
            this.SignsPanel.TabIndex = 32;
            // 
            // NPagForDoc
            // 
            this.NPagForDoc.Location = new System.Drawing.Point(120, 43);
            this.NPagForDoc.Name = "NPagForDoc";
            this.NPagForDoc.Size = new System.Drawing.Size(120, 20);
            this.NPagForDoc.TabIndex = 35;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 13);
            this.label4.TabIndex = 34;
            this.label4.Text = "N. Pag. Singolo Doc.";
            // 
            // PrintPanel
            // 
            this.PrintPanel.Controls.Add(this.button2);
            this.PrintPanel.Controls.Add(this.button1);
            this.PrintPanel.Controls.Add(this.ToPRintXml);
            this.PrintPanel.Controls.Add(this.FileToPrint);
            this.PrintPanel.Controls.Add(this.printerNameBox);
            this.PrintPanel.Controls.Add(this.label7);
            this.PrintPanel.Controls.Add(this.label6);
            this.PrintPanel.Controls.Add(this.label5);
            this.PrintPanel.Location = new System.Drawing.Point(255, 222);
            this.PrintPanel.Name = "PrintPanel";
            this.PrintPanel.Size = new System.Drawing.Size(722, 88);
            this.PrintPanel.TabIndex = 33;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(644, 58);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "Browse";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(644, 7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Browse";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ToPRintXml
            // 
            this.ToPRintXml.Location = new System.Drawing.Point(126, 60);
            this.ToPRintXml.Name = "ToPRintXml";
            this.ToPRintXml.Size = new System.Drawing.Size(498, 20);
            this.ToPRintXml.TabIndex = 5;
            // 
            // FileToPrint
            // 
            this.FileToPrint.Location = new System.Drawing.Point(126, 9);
            this.FileToPrint.Name = "FileToPrint";
            this.FileToPrint.Size = new System.Drawing.Size(498, 20);
            this.FileToPrint.TabIndex = 4;
            // 
            // printerNameBox
            // 
            this.printerNameBox.FormattingEnabled = true;
            this.printerNameBox.Location = new System.Drawing.Point(126, 30);
            this.printerNameBox.Name = "printerNameBox";
            this.printerNameBox.Size = new System.Drawing.Size(498, 21);
            this.printerNameBox.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 63);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(117, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "File accompagnamento";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Stampante";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Nome File";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modificaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(996, 24);
            this.menuStrip1.TabIndex = 34;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // modificaToolStripMenuItem
            // 
            this.modificaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configuraNuovaStampanteToolStripMenuItem});
            this.modificaToolStripMenuItem.Name = "modificaToolStripMenuItem";
            this.modificaToolStripMenuItem.Size = new System.Drawing.Size(97, 20);
            this.modificaToolStripMenuItem.Text = "Configurazioni";
            // 
            // configuraNuovaStampanteToolStripMenuItem
            // 
            this.configuraNuovaStampanteToolStripMenuItem.Name = "configuraNuovaStampanteToolStripMenuItem";
            this.configuraNuovaStampanteToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.configuraNuovaStampanteToolStripMenuItem.Text = "Configura nuova stampante";
            this.configuraNuovaStampanteToolStripMenuItem.Click += new System.EventHandler(this.configuraNuovaStampanteToolStripMenuItem_Click);
            // 
            // PdfMergePanel
            // 
            this.PdfMergePanel.Controls.Add(this.button4);
            this.PdfMergePanel.Controls.Add(this.Browse);
            this.PdfMergePanel.Controls.Add(this.PdfMergeOutput);
            this.PdfMergePanel.Controls.Add(this.PdfMergeInput);
            this.PdfMergePanel.Controls.Add(this.label9);
            this.PdfMergePanel.Controls.Add(this.label8);
            this.PdfMergePanel.Location = new System.Drawing.Point(255, 317);
            this.PdfMergePanel.Name = "PdfMergePanel";
            this.PdfMergePanel.Size = new System.Drawing.Size(722, 73);
            this.PdfMergePanel.TabIndex = 35;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(644, 37);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 9;
            this.button4.Text = "Browse";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Browse
            // 
            this.Browse.Location = new System.Drawing.Point(644, 14);
            this.Browse.Name = "Browse";
            this.Browse.Size = new System.Drawing.Size(75, 23);
            this.Browse.TabIndex = 8;
            this.Browse.Text = "Browse";
            this.Browse.UseVisualStyleBackColor = true;
            this.Browse.Click += new System.EventHandler(this.Browse_Click);
            // 
            // PdfMergeOutput
            // 
            this.PdfMergeOutput.Location = new System.Drawing.Point(134, 39);
            this.PdfMergeOutput.Name = "PdfMergeOutput";
            this.PdfMergeOutput.Size = new System.Drawing.Size(491, 20);
            this.PdfMergeOutput.TabIndex = 5;
            // 
            // PdfMergeInput
            // 
            this.PdfMergeInput.Location = new System.Drawing.Point(134, 16);
            this.PdfMergeInput.Name = "PdfMergeInput";
            this.PdfMergeInput.Size = new System.Drawing.Size(491, 20);
            this.PdfMergeInput.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 42);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "File Output";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Cartella Input";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(996, 490);
            this.Controls.Add(this.PdfMergePanel);
            this.Controls.Add(this.PrintPanel);
            this.Controls.Add(this.SignsPanel);
            this.Controls.Add(this.MergePanel);
            this.Controls.Add(this.TemplatePanel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonMergeANDSegni);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "MainWindow";
            ((System.ComponentModel.ISupportInitialize)(this.fieldRequired)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.TemplatePanel.ResumeLayout(false);
            this.TemplatePanel.PerformLayout();
            this.MergePanel.ResumeLayout(false);
            this.MergePanel.PerformLayout();
            this.SignsPanel.ResumeLayout(false);
            this.SignsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NPagForDoc)).EndInit();
            this.PrintPanel.ResumeLayout(false);
            this.PrintPanel.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.PdfMergePanel.ResumeLayout(false);
            this.PdfMergePanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label TemplateFile;
        private System.Windows.Forms.Label ExcelFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox TemplateFileBox;
        private System.Windows.Forms.TextBox ExcelFileBox;
        private System.Windows.Forms.Button buttonTempalteFile;
        private System.Windows.Forms.Button buttonExcelFile;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox DestinationXMLFileName;
        private System.Windows.Forms.Button buttonMergeANDSegni;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox destinationPDFFileName;
        private System.Windows.Forms.Button buttonPDFMerge;
        private System.Windows.Forms.Button buttonXLMerge;
        private System.Windows.Forms.Button buttonPDFSigns;
        private System.Windows.Forms.TextBox signsPDFFileName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ErrorProvider fieldRequired;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button PrintMenu;
        private System.Windows.Forms.Button PrintSignsMenu;
        private System.Windows.Forms.Button MailMergeMenu;
        private System.Windows.Forms.Panel TemplatePanel;
        private System.Windows.Forms.Panel SignsPanel;
        private System.Windows.Forms.Panel MergePanel;
        private System.Windows.Forms.Panel PrintPanel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox ToPRintXml;
        private System.Windows.Forms.TextBox FileToPrint;
        private System.Windows.Forms.ComboBox printerNameBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem modificaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configuraNuovaStampanteToolStripMenuItem;
        private System.Windows.Forms.NumericUpDown NPagForDoc;
        private System.Windows.Forms.Label label4;
        private Button menuButtonPdfMerge;
        private Panel PdfMergePanel;
        private TextBox PdfMergeOutput;
        private TextBox PdfMergeInput;
        private Label label9;
        private Label label8;
        private Button button4;
        private Button Browse;
    }
}

